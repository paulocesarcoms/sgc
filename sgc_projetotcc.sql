-- MySQL dump 10.15  Distrib 10.0.31-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sgc_projetotcc
-- ------------------------------------------------------
-- Server version	10.0.31-MariaDB-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_consultas`
--

DROP TABLE IF EXISTS `tb_consultas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_consultas` (
  `consId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pessIdMed` int(10) unsigned DEFAULT NULL,
  `pessIdPas` int(10) unsigned DEFAULT NULL,
  `consDt` datetime DEFAULT NULL,
  `consStatus` smallint(6) DEFAULT '0',
  PRIMARY KEY (`consId`),
  KEY `FK_tb_consultas_tb_pessoas` (`pessIdMed`),
  KEY `FK_tb_consultas_tb_pessoas_2` (`pessIdPas`),
  CONSTRAINT `FK_tb_consultas_tb_pessoas` FOREIGN KEY (`pessIdMed`) REFERENCES `tb_pessoas` (`pessId`),
  CONSTRAINT `FK_tb_consultas_tb_pessoas_2` FOREIGN KEY (`pessIdPas`) REFERENCES `tb_pessoas` (`pessId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_consultas`
--

LOCK TABLES `tb_consultas` WRITE;
/*!40000 ALTER TABLE `tb_consultas` DISABLE KEYS */;
INSERT INTO `tb_consultas` VALUES (1,12,13,'2017-08-22 11:11:00',1),(2,12,NULL,'2017-10-22 11:30:00',1),(3,12,NULL,'2017-10-23 01:11:00',1),(4,12,8,'2017-09-19 12:00:00',1),(6,14,15,'2017-09-06 09:00:00',1),(7,14,15,'2017-09-19 09:00:00',1),(8,14,NULL,'2017-10-23 12:00:00',1),(21,14,NULL,'2017-10-24 10:00:00',1);
/*!40000 ALTER TABLE `tb_consultas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_consultas_exames`
--

DROP TABLE IF EXISTS `tb_consultas_exames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_consultas_exames` (
  `consExaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `consId` int(10) unsigned NOT NULL,
  `exaId` int(10) unsigned DEFAULT NULL,
  `consExaDesc` text,
  `consExaDestino` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`consExaId`),
  KEY `FK_tb_consultas_exames_tb_consultas` (`consId`),
  KEY `FK_tb_consultas_exames_tb_exames` (`exaId`),
  CONSTRAINT `FK_tb_consultas_exames_tb_consultas` FOREIGN KEY (`consId`) REFERENCES `tb_consultas` (`consId`),
  CONSTRAINT `FK_tb_consultas_exames_tb_exames` FOREIGN KEY (`exaId`) REFERENCES `tb_exames` (`exaId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_consultas_exames`
--

LOCK TABLES `tb_consultas_exames` WRITE;
/*!40000 ALTER TABLE `tb_consultas_exames` DISABLE KEYS */;
INSERT INTO `tb_consultas_exames` VALUES (1,4,1,'',NULL);
/*!40000 ALTER TABLE `tb_consultas_exames` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_consultas_medicamentos`
--

DROP TABLE IF EXISTS `tb_consultas_medicamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_consultas_medicamentos` (
  `consMedId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `medId` int(10) unsigned NOT NULL,
  `consId` int(10) unsigned NOT NULL,
  `consMedDesc` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`consMedId`),
  KEY `FK_tb_consultas_medicamentos_tb_medicamentos` (`medId`),
  KEY `FK_tb_consultas_medicamentos_tb_consultas` (`consId`),
  CONSTRAINT `FK_tb_consultas_medicamentos_tb_consultas` FOREIGN KEY (`consId`) REFERENCES `tb_consultas` (`consId`) ON DELETE CASCADE,
  CONSTRAINT `FK_tb_consultas_medicamentos_tb_medicamentos` FOREIGN KEY (`medId`) REFERENCES `tb_medicamentos` (`medId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_consultas_medicamentos`
--

LOCK TABLES `tb_consultas_medicamentos` WRITE;
/*!40000 ALTER TABLE `tb_consultas_medicamentos` DISABLE KEYS */;
INSERT INTO `tb_consultas_medicamentos` VALUES (1,1,4,''),(2,2,4,'');
/*!40000 ALTER TABLE `tb_consultas_medicamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_endereco`
--

DROP TABLE IF EXISTS `tb_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_endereco` (
  `endId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `engLogradouro` varchar(512) DEFAULT NULL,
  `engCidade` varchar(512) DEFAULT NULL,
  `endEstado` varchar(512) DEFAULT NULL,
  `endCep` varchar(50) DEFAULT NULL,
  `endStatus` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`endId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_endereco`
--

LOCK TABLES `tb_endereco` WRITE;
/*!40000 ALTER TABLE `tb_endereco` DISABLE KEYS */;
INSERT INTO `tb_endereco` VALUES (1,'','eeee','','',1),(2,'',NULL,NULL,NULL,1),(3,NULL,NULL,NULL,NULL,1),(4,NULL,NULL,NULL,NULL,1),(5,NULL,NULL,NULL,NULL,1),(6,'rua olipio','Montes Claros','','13123',1),(7,'@@@','','','',1),(8,'','','','',1),(9,'','','','',1),(10,'','','','',1),(11,'','','','',1),(12,'','','','',1),(13,'','','','',1),(14,'','','','',1),(15,'','','','',1),(16,'','','','',1),(17,'','','','',1),(18,NULL,NULL,NULL,NULL,1),(19,NULL,NULL,NULL,NULL,1),(20,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `tb_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_exames`
--

DROP TABLE IF EXISTS `tb_exames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_exames` (
  `exaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exaNome` varchar(255) NOT NULL,
  PRIMARY KEY (`exaId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_exames`
--

LOCK TABLES `tb_exames` WRITE;
/*!40000 ALTER TABLE `tb_exames` DISABLE KEYS */;
INSERT INTO `tb_exames` VALUES (1,'HEMOGRAMA'),(2,'COLESTEROL'),(3,'UREIA e CREATININA'),(4,'GLICOSE'),(5,'TGO (AST) TGP (ALP)'),(6,'TSH e T4 livre'),(7,'ÁCIDO ÚRICO'),(8,'PCR'),(9,'PSA'),(10,'ALBUMINA'),(11,'VHS ou VS'),(12,'EAS ou Urina Tipo I'),(13,'UROCULTURA'),(14,'EXAME PARASITOLÓGICO DE FEZES');
/*!40000 ALTER TABLE `tb_exames` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_grupos`
--

DROP TABLE IF EXISTS `tb_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_grupos` (
  `grId` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `grNome` varchar(255) NOT NULL,
  PRIMARY KEY (`grId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_grupos`
--

LOCK TABLES `tb_grupos` WRITE;
/*!40000 ALTER TABLE `tb_grupos` DISABLE KEYS */;
INSERT INTO `tb_grupos` VALUES (1,'pacientes'),(2,'Funcionarios'),(3,'recepcionista');
/*!40000 ALTER TABLE `tb_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_medicamentos`
--

DROP TABLE IF EXISTS `tb_medicamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_medicamentos` (
  `medId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `medNomeGen` varchar(255) DEFAULT NULL,
  `medNomeFab` varchar(255) DEFAULT NULL,
  `medStatus` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`medId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_medicamentos`
--

LOCK TABLES `tb_medicamentos` WRITE;
/*!40000 ALTER TABLE `tb_medicamentos` DISABLE KEYS */;
INSERT INTO `tb_medicamentos` VALUES (1,'Paracetamol','',1),(2,'Loratadina 10 mg','Loratamed',1),(3,'Diclofenaco sódico','',1);
/*!40000 ALTER TABLE `tb_medicamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_medicos`
--

DROP TABLE IF EXISTS `tb_medicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_medicos` (
  `medId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pessId` int(10) unsigned NOT NULL,
  `medCrm` varchar(255) DEFAULT NULL,
  `medEsp` varchar(512) DEFAULT NULL COMMENT 'Especialidades',
  PRIMARY KEY (`medId`),
  KEY `FK_tb_medicos_tb_pessoas` (`pessId`),
  CONSTRAINT `FK_tb_medicos_tb_pessoas` FOREIGN KEY (`pessId`) REFERENCES `tb_pessoas` (`pessId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_medicos`
--

LOCK TABLES `tb_medicos` WRITE;
/*!40000 ALTER TABLE `tb_medicos` DISABLE KEYS */;
INSERT INTO `tb_medicos` VALUES (1,12,'123245435','pediatra'),(2,14,'',''),(4,17,'','');
/*!40000 ALTER TABLE `tb_medicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_permissoes`
--

DROP TABLE IF EXISTS `tb_permissoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permissoes` (
  `permId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `permName` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`permId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_permissoes`
--

LOCK TABLES `tb_permissoes` WRITE;
/*!40000 ALTER TABLE `tb_permissoes` DISABLE KEYS */;
INSERT INTO `tb_permissoes` VALUES (1,'administrador'),(2,'medico'),(3,'recepcionista');
/*!40000 ALTER TABLE `tb_permissoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_permissoes_usuario`
--

DROP TABLE IF EXISTS `tb_permissoes_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permissoes_usuario` (
  `permUserId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permId` mediumint(8) unsigned DEFAULT NULL,
  `userId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`permUserId`),
  KEY `FK_ws_permissions_user_ws_permissions` (`permId`),
  KEY `FK_ws_permissions_user_ws_user` (`userId`),
  CONSTRAINT `FK_ws_permissions_user_ws_permissions` FOREIGN KEY (`permId`) REFERENCES `tb_permissoes` (`permId`) ON DELETE CASCADE,
  CONSTRAINT `FK_ws_permissions_user_ws_user` FOREIGN KEY (`userId`) REFERENCES `tb_usuario` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_permissoes_usuario`
--

LOCK TABLES `tb_permissoes_usuario` WRITE;
/*!40000 ALTER TABLE `tb_permissoes_usuario` DISABLE KEYS */;
INSERT INTO `tb_permissoes_usuario` VALUES (10,3,4),(12,2,3),(13,2,6),(14,1,1),(15,1,2);
/*!40000 ALTER TABLE `tb_permissoes_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoas`
--

DROP TABLE IF EXISTS `tb_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoas` (
  `pessId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pessNome` varchar(255) DEFAULT NULL,
  `pessCpf` varchar(11) DEFAULT NULL,
  `pessDtNasc` datetime DEFAULT NULL,
  `pessRg` varchar(50) DEFAULT NULL,
  `pessNomeMae` varchar(255) DEFAULT NULL,
  `pessNomePai` varchar(255) DEFAULT NULL,
  `pessNat` varchar(255) DEFAULT NULL,
  `endId` int(10) unsigned DEFAULT NULL,
  `pessStatus` smallint(6) NOT NULL DEFAULT '0',
  `userId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`pessId`),
  KEY `FK_tb_pessoas_tb_endereco` (`endId`),
  KEY `FK_tb_pessoas_tb_usuario` (`userId`),
  CONSTRAINT `FK_tb_pessoas_tb_endereco` FOREIGN KEY (`endId`) REFERENCES `tb_endereco` (`endId`),
  CONSTRAINT `FK_tb_pessoas_tb_usuario` FOREIGN KEY (`userId`) REFERENCES `tb_usuario` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoas`
--

LOCK TABLES `tb_pessoas` WRITE;
/*!40000 ALTER TABLE `tb_pessoas` DISABLE KEYS */;
INSERT INTO `tb_pessoas` VALUES (1,'Paulo','3333',NULL,'','',NULL,NULL,1,1,NULL),(2,'Administrador',NULL,NULL,NULL,NULL,NULL,NULL,2,1,2),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,0,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,0,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,0,NULL),(6,'Jon','',NULL,'','',NULL,NULL,6,1,NULL),(7,'FFF','',NULL,'','',NULL,NULL,7,1,NULL),(8,'Eduardo Fagundes','1111111111',NULL,'55.555.555','Rosária Fagundes',NULL,NULL,8,1,NULL),(9,'','',NULL,'','',NULL,NULL,9,-1,NULL),(10,'Usuario pp','',NULL,'','',NULL,NULL,10,1,NULL),(11,'Daniele Silva','',NULL,'','',NULL,NULL,11,1,4),(12,'Paulo Roberto','',NULL,'','',NULL,NULL,12,1,NULL),(13,'Flávio Souza','00000000000',NULL,'22.222.222','Catarina Souza',NULL,NULL,13,1,NULL),(14,'Messias','',NULL,'','',NULL,NULL,14,1,3),(15,'Pedro Antônio','00000000000',NULL,'77.777.777','Cleusa ',NULL,NULL,15,1,NULL),(17,'Mariana','',NULL,'','',NULL,NULL,17,1,6),(20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,0,NULL);
/*!40000 ALTER TABLE `tb_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoas_contato`
--

DROP TABLE IF EXISTS `tb_pessoas_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoas_contato` (
  `pcContId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pessId` int(10) unsigned NOT NULL,
  `tContId` smallint(5) unsigned NOT NULL,
  `pContato` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pcContId`),
  KEY `FK_tb_pessoas_contato_tb_pessoas` (`pessId`),
  CONSTRAINT `FK_tb_pessoas_contato_tb_pessoas` FOREIGN KEY (`pessId`) REFERENCES `tb_pessoas` (`pessId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoas_contato`
--

LOCK TABLES `tb_pessoas_contato` WRITE;
/*!40000 ALTER TABLE `tb_pessoas_contato` DISABLE KEYS */;
INSERT INTO `tb_pessoas_contato` VALUES (1,8,1,''),(2,9,1,'111'),(3,10,2,'ppppp@dfs.com'),(4,11,2,''),(5,12,1,''),(6,8,2,'');
/*!40000 ALTER TABLE `tb_pessoas_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoas_grupos`
--

DROP TABLE IF EXISTS `tb_pessoas_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoas_grupos` (
  `grId` smallint(5) unsigned NOT NULL,
  `pessId` int(10) unsigned NOT NULL,
  KEY `FK_tb_pessoas_grupos_tb_grupos` (`grId`),
  KEY `FK_tb_pessoas_grupos_tb_pessoas` (`pessId`),
  CONSTRAINT `FK_tb_pessoas_grupos_tb_grupos` FOREIGN KEY (`grId`) REFERENCES `tb_grupos` (`grId`) ON DELETE CASCADE,
  CONSTRAINT `FK_tb_pessoas_grupos_tb_pessoas` FOREIGN KEY (`pessId`) REFERENCES `tb_pessoas` (`pessId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoas_grupos`
--

LOCK TABLES `tb_pessoas_grupos` WRITE;
/*!40000 ALTER TABLE `tb_pessoas_grupos` DISABLE KEYS */;
INSERT INTO `tb_pessoas_grupos` VALUES (1,8),(2,9),(2,11),(2,12),(1,13),(2,14),(1,15),(2,17),(1,20);
/*!40000 ALTER TABLE `tb_pessoas_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_contato`
--

DROP TABLE IF EXISTS `tb_tipo_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_contato` (
  `tContId` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `tContNome` varchar(50) DEFAULT NULL,
  `tContTipo` varchar(50) NOT NULL,
  PRIMARY KEY (`tContId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_contato`
--

LOCK TABLES `tb_tipo_contato` WRITE;
/*!40000 ALTER TABLE `tb_tipo_contato` DISABLE KEYS */;
INSERT INTO `tb_tipo_contato` VALUES (1,'Telefone','phone'),(2,'Email','email'),(3,'WhatsApp','phone'),(4,'Skype','text'),(5,'Facebook','text'),(6,'Twitter','text');
/*!40000 ALTER TABLE `tb_tipo_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userStatus` smallint(5) DEFAULT NULL,
  `userType` varchar(50) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `userEmail` varchar(255) DEFAULT NULL,
  `userLogin` varchar(255) DEFAULT NULL,
  `userPassword` varchar(510) DEFAULT NULL,
  `userSuper` tinyint(1) DEFAULT NULL,
  `userDtCreate` datetime DEFAULT NULL,
  `userDtModified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userLastLogin` datetime DEFAULT NULL,
  `userLastBrowser` varchar(255) DEFAULT NULL,
  `userLastIp` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userLogin` (`userLogin`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario`
--

LOCK TABLES `tb_usuario` WRITE;
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` VALUES (1,1,NULL,'Paulo','','paulo-cesar','296c28415710cef1883ea25bc1632405e24435908984819f9bcad5b1e07b8fb0718624bd0b89c17fdd7fadf64974e0c8d2157221c19fc97dc275938d9d63b094M+oWDHq6VaYbwTvTODX8MijglmIwWqeK9RIMJNpjIzY=',1,'2016-01-13 14:11:59','2017-09-20 00:12:15','2017-09-19 21:12:15','Chrome 61.0.3163.79','::1'),(2,1,NULL,'Admin','','admin','31326c6015faf622d426120c768313e38277fa7ebeea01f5cd11a37330ae6313e40d3594e09ad1bbbbae6d1854a4109473380ba58236094514825540098ccca5SLsYnasCSH9k+7qpMhVJbS1KlOJA869nRMpSLR3EDW4=',NULL,'2016-01-14 09:32:11','2017-09-20 01:48:35','2017-09-19 22:48:35','Chrome 61.0.3163.79','::1'),(3,1,NULL,NULL,NULL,'doutor-messias','5c8fa12a2690745835bf408e7f4d3ca0546a49a1c860c5b328049b594b67d73916bb867c1a4dd549c904ec90392938aed1c61fa12887534dfd6c38e2b723b774IBDl9wieDeE/SSc3gk9+ZmR9cDE++DTxJRUNm/mbYCM=',NULL,NULL,'2017-09-19 23:47:55','2017-09-19 20:47:55','Chrome 61.0.3163.79','::1'),(4,1,NULL,NULL,NULL,'recepcionista','6a3451899d51597b7a71851d6d3dab0dcea1d72c90c546d545340c8beb531fd6271e47798c0152c38a0db8726846d2271deb8a83098166447453e9d365c47b55CjVOTr5hQKi9vRHB6Y999qLn9FrZ/c4TJemyeuS2gyQ=',NULL,'2017-08-12 22:35:56','2017-09-20 00:42:00','2017-09-19 21:42:00','Chrome 61.0.3163.79','::1'),(6,1,NULL,NULL,NULL,'doutora-mariana','3081d1504bb43a61f054f2d5022ab12a024b474e64dfb82301867fd9377b3221a201e9a66531cd41d7298fe2888d02ebba53de08152826a1af37ad20f52344b8TO8Be84+TIPPxpwzElXN3p72LD19amyzcf4p/9IBEVg=',NULL,'2017-09-10 18:05:07','2017-09-18 01:02:23',NULL,NULL,NULL),(7,0,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-19 20:50:39','2017-09-19 23:50:39',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 22:50:39
