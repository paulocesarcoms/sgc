function parse_json(data){
    if(typeof data != "object"){
        if(typeof JSON == 'object'){
            data = JSON.parse(data);
        }else{
            data = $.parseJSON(data);
        }
    }
    return data;
}
function ws_alert(text ,type ,data ){
    var title = '';
    if(!type){ type = 'info' }
    if(type == 'error'){ type = 'danger' }
    if(!data){ data = {} }
    if(data.title){
        title = data.title;
    }
    var msg = $('\
    <div class="ws-alert alert alert-'+type+'" role="alert">\
    <button type="button" class="close" data-dismiss="alert" ><span aria-hidden="true">&times;</span></button>\
    <strong>'+title+'</strong> '+text+'\
    </div>');
    if(data.time){
        setTimeout(function(){ msg.remove() },(data.time * 1000) );
    }
    $(document.body).append(msg);
}
function process_response_messages(data){
    if(data.length){
        for(var m in data){
            if(data[m].messages){
                var messages = data[m].messages;
                for(var i in messages){
                    ws_alert(messages[i].content, messages[i].type);
                }
            }
        }
    }
}
var WS = {
    behaviors: {},
    base_url:base_url,
    load: function(context) {
        for(var behavior in this.behaviors) {
            this.behaviors[behavior](context);
        }
    },
    process_request:function(data) {
        if(data.contents){         
        	process_response_messages(data);        	
            for(var index in data.contents) {
                var content = data.contents[index];
                var c = $(content.content);
                $(content.destination)[content.action](c);
                WS.load(c);
            }
        }else{
            for(var i in data) {
                if(data[i].content){
                    var content = $(data[i].content);
                    var selector = data[i].selector;
                    if((!selector || selector.length==0) && content.attr('id')){
                        selector = '#'+content.attr('id');
                    }
                    if(selector){
                        $(selector)[data[i].function](content);
                        WS.load(content);
                    }
                    	console.log(selector)
                }
            }
        }
    }
}
WS.behaviors.agc = function(context){
	
	$('.form-xhr',context).submit(function(event){
	    var action = $(this).attr('action');	    
	    if(typeof CKEDITOR == 'object'){            
	        $('textarea').each(function() {
	            if(this.id && typeof CKEDITOR.instances[this.id] == 'object' ){
	                CKEDITOR.instances[this.id].updateElement();
	            }
	        });
	    }
	    
	    var that =  $(this);
	    var options = {
	        url:action,
	        method:'post',	      
	        success:function(data,s,obj){                
	            data = parse_json(data);
	            process_response_messages(data);
	            WS.process_request(data);
	            that.trigger('success_submit');
	        },
	        error:function(data){
	            if(data.status == 401 && data.responseText.match(/^{.*}$/)){
	                var resp = parse_jsonN(data.responseText);
	                if(typeof resp.redirect  == 'string'){
	                    location.href = WS.base_url(resp.redirect);
	                }
	            }
	        }

	    };
	    var files =  $('[type="file"]',this);
	    if(files.length && files.val() !='' && typeof FormData == 'function'){
	        var formData = new FormData(this);
	        options.data = formData;
	        options.contentType = false;
	        options.processData = false;
	    }else{
	        options.data = $(this).serialize();
	    }
	    try{
	        $.ajax(options);
	    }catch(e){
	        if(typeof console == 'object' && typeof console.error == 'function'){
	            console.error(e);
	        }
	    }
	    return false;
	}).on('progress_post',function(e,frac){
	    var progress = $(this).find('.progress-group');
	    if(progress.length == 0){                
	        progress = $( 
	        '<div class="progress-group">'
	            +'<span class="progress-text">&nbsp;</span>'
	            +'<span class="progress-number"><b>0</b>/100%</span>'
	            +'<div class="progress sm">'
	                +'<div class="progress-bar progress-bar-aqua" style="width:0%"></div>'
	            +'</div>'
	        +'</div>');
	        progress.css('display','none');
	        $(this).prepend(progress);
	        progress.fadeIn();
	    }
	    procent = (frac * 100);      
	    progress.find('.progress-bar').css('width',procent+'%');
	    progress.find('.progress-number').html('<b>'+procent+'</b>/100%');        

	});
	var strComp = ' <div class="form-group col-lg-6">'
	    +'<div class="input-group">'
	      +'<div class="input-group-addon"></div>'
	      +'<input type="text" class="form-control">'
	      +'<div class="input-group-addon trash link"><span class=" glyphicon glyphicon-trash"></span></div>'
	    +'</div>'
	  +'</div>';
	 var func_trash = function(el){
	 	if(el.length == 0){
	 		return false;
	 	}
	 	$(el).each(function(a,b){
		 	var that = $(b);	 	
		 	var name = that.attr('name');
		 	if(!name || name.match(/_trash/)){
		 		return false;
		 	}
		 	var mt = name.match(/\[(\d+)\]$|\[(\d+)\]\[\]$|([^\[]+)(.*)/);
		 	that.attr('data-name',name);
		 	if(mt){		 		
			 	if(mt[1]){
			 		that.attr('name',name.replace(/^([^\[]+).*/,'$1')+'_trash['+mt[1]+']');
			 	}else if(mt[2]){
			 		that.attr('name','');
			 	}else if(mt[3]){
			 		if(mt[3].match(/_new$/)){
			 			that.attr('name','');
			 		}else{
			 			that.attr('name',mt[3]+'_trash'+mt[4]);
			 		}
			 	}
		 	}
	 	});

	 	var war = $('<label  class="control-label link">Desfazer remover</label>');
	 	war.click(function(){
	 		$(el).each(function(a,b){
	 			var that = $(b);
		 		that.closest('.form-group').removeClass('has-warning');
		 		that.attr('name',that.attr('data-name'));
		 		war.remove();
	 		});
	 	})
	 	$(el[0]).closest('.form-group').addClass('has-warning').prepend(war);
	 	
	}

	$('.trash',context).on('click',function(){
		var selec = $(this).attr('data-fields');
		if(!selec){
			selec = '>input';
		}
		func_trash($(this).parent().find(selec));
	});
	$('[data-tcid]',context).click(function(){
		var that = $(this);
		var comp = $(strComp);
		var tcid = that.attr('data-tcid');
		if(tcid){
			comp.find('input').attr('name','contact_new['+tcid+'][]').attr('type',that.attr('data-type'));
			comp.find('.input-group-addon:first').html(that.text());
			comp.insertAfter(that.closest('.input-group'));
			comp.find('.trash').click(function(){
				var selec = $(this).attr('data-fields');
				if(!selec){
					selec = '>input';
				}
				func_trash($(this).parent().find(selec));
			});
		}
		
	});
	$('[name="consDt[date]"]',context).mask('00/00/0000');
	$('[name="consDt[time]"]',context).mask('00:00');
	
	$('.ws-window',context).on('click',function(){
        var title = $(this).attr('title');
        if(title == undefined){
            title = $(this).text();
        }
        var idModal = $(this).attr('data-modal-id');
        if( typeof idModal == 'undefined'){
            idModal = '';
        }else{
            idModal = 'id="'+idModal+'"';
        } 
        var cssClass = $(this).attr('data-modal-class'); 
        if(cssClass == undefined){
            cssClass = '';
        }
        var url = $(this).attr('data-modal-href');
        if( typeof url == 'undefined' ){
            url = $(this).attr('href');
        }

        var matchH = url.match(/\?.*height *= *(\d+)/);
        var matchW = url.match(/\?.*width *= *(\d+)/);
        var mw;
        if(matchW){
            mw = matchW[1];
        }
        if(!mw){
            if( typeof $(this).attr('data-modal-width') == 'string' ){
                mw = $(this).attr('data-modal-width');
            }
        }
        var m =   $('<div '+idModal+' class="modal fade '+cssClass+' ">  <div class="modal-dialog">  <div class="modal-content"> <div class="modal-header">   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   <h4 class="modal-title">'+title+'</h4>  </div>   <div class="modal-body">  </div>   </div>  </div></div>');
        m.data('elStart',this);
        var body = $('body');
        if(mw){
            $('.modal-dialog',m).css('width',mw).css('max-width','100%');
        }
        
        var m_body =  $('.modal-body',m);
        if(url.match(/\.jpg|\.png|\.gif$/)){
            m_body.addClass('img');
            m_body.html('');
            m_body.html('<img src="'+url+'"/>');                
        }else{
            $.ajax({
                url: url,
                success:function(data){
                    data = parse_json(data);
                    m_body.html('');
                    for(var i in data){
                        var c = $(data[i].content);
                        WS.load(c);
                        m_body.append(c);
                        c.trigger('inserted');
                    }
                }
            });
        }
        

        m.data('body_css',{ overflow: body.get(0).style.overflow, position: body.get(0).style.position, height: body.get(0).style.height });
        
        m.data('win_scrollTop',$( window ).scrollTop());
        m.one('hide.bs.modal',function(){
            var body_css = m.data('body_css');
            //body.css('overflow',body_css.overflow).css('position',body_css.position);            
            //$(window).scrollTop(m.data('win_scrollTop'));            
        });
        m.one('hidden.bs.modal',function(){
            $(this).remove();
        });
        m.one('show.bs.modal',function(){
            //body.css('overflow','hidden').css('position','fixed');
        });
        $('.modal-header .close',m).click(function(){
            m.modal('hide');
        });
        m.modal('show');
        return false;
    });
	$('.form-autoloading, .form-busca',context).on('input',function(){
		if(ctrlAutoloading){
			ctrlAutoloading = false;
			var el = this
			setTimeout(function(){ autoloading(el); },500);	
		}
		if($(this).find('.loader').length == 0){
			$('<span class="loader" ></span>').insertBefore($(this).find('.glyphicon-search'));
		}
	}).submit(function(){ return false ;});

	$('li.item-autoloading',context).click(function(){
		var m = $($(this).closest('.modal'));
		var el = m.data('elStart');
		if(el){
			$(el).parent().find('input[type="hidden"]:first').val($(this).attr('data-id'));
			$(el).parent().find('.text').text($(this).text());
		}
		m.find('.close').trigger('click');
	});
	$('.medi-add',context).click(function(){
		var prot = $(this).parent().find('.prototipo');
		if(prot.length){
			var clone = prot.clone(true);
			clone.removeClass('prototipo');
			prot.parent().append(clone);
			clone.find('.ws-window').trigger('click');
		}
	});
	$('[data-refresh]',context).on('refresh',function(){
		var url = $(this).attr('data-refresh');
		$.ajax({
	        url: url,
	        success:function(data){
				WS.process_request(data);
	        }
	     });
	});
	$('.remover',context).popover({
		placement:'top',
		html:true,
		content:function(){
			var c = '<div>Tem certeza que deseja remover esse item?<center>';
			c = c + '<button type="button" class="btn btn-danger btn-sm sim">Sim</button> ';
			c = c + '<button type="button" class="btn btn-default btn-sm nao">Não</button>';
			c =c + '</center><div>';
			c = $(c);
			var that = this;
			c.find('.nao').click(function(){
				$(that).trigger('click');
			});
			c.find('.sim').click(function(){
				var url = $(that).attr('href');
				$.ajax({
                url: url,
	                success:function(data){
	                    if(data.length){
					        for(var m in data){
					            if(data[m].variables && data[m].variables.error){
	                    			process_response_messages(data);
					                return;
					                break;
					            }
					        }
					    }
					    var b = $(that).closest('tr');
					    if(b.length){
					    	b.remove();
					    }else{
					    	$(that).closest('[data-refresh]').trigger('refresh');
					    }
					    
	                }
            	});
			});
			return c;
		}
	});
	$('.navbar-toggle',context).click(function(){
		var b = $('body');
		if(b.hasClass('sidebar-open')){
			b.removeClass('sidebar-open');
		}else{
			b.addClass('sidebar-open');
		}
	})
	
};
ctrlAutoloading = true;
function autoloading(el){
		if(!ctrlAutoloading){
			ctrlAutoloading = true;
			setTimeout(function(){ autoloading(el); },500);
			return false;
		}
		el = $(el);
		var method = el.attr('method');
		if(!method){
			method = 'post';
		}
		$.ajax({
			method:method,
			url: el.attr('action'),
			data:el.serialize(),
			success:function(data){
				WS.process_request(data);
				el.find('.loader:last').remove()
			}
		});
}
function selectmenus(){
    var arrPathname = location.pathname.split('/');
    var re  = new RegExp('^'+WS.base_url()+'?$');
    for(var i = 0; i < arrPathname.length; i++){
        var href = i == 0 ? location.href : location.origin+arrPathname.slice(0,-i).join('/');
        var linkM = $('.container-menu li a[href^="'+href+'"]');
        if(href.match(re)){
            break;
        }
        if(linkM.length){
            linkM.first().closest('li').addClass('active');
            break;
        }
    };

    
}
$(function () {
	WS.load(document);
});
selectmenus();