<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Encryption extends CI_Encryption {
	public function __construct(array $params = array()){
		/*if(!array_key_exists('driver', $params) && defined('MCRYPT_DEV_URANDOM')){
			$params['driver'] = 'mcrypt';
		}*/
		if(!array_key_exists('driver', $params)){
			$params['driver'] = 'openssl';
		}
        return parent::__construct($params);
    }
}