<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Classe para load dinamico de objetos Models
*/
class M{	
	function __get($model){
		if(isset($this->{$model})){
			return $this->{$model};
		}
		
		$CI = & get_instance();
		if(empty($CI->{$model})){
			$CI->load->model($model);
		}

		$this->{$model} = & $CI->{$model};
		return $this->{$model};
	}
}