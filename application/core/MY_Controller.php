<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	var $theme = '';
	var $sub_theme = '';
	var $segment_base = '';
	var $data_response_json = [];
    public function __construct(){
    	parent::__construct();
        $this->session->set_userdata('last_url',current_url());
        $this->check_logged_in();
        if(!empty($this->ModelNome)){
            $this->load->model($this->ModelNome);
            $this->objModel = & $this->{$this->ModelNome};
        }
    }    
    public function response_json($data = [],$selector = '',$message = '', $function = 'replaceWith'){
    	$resp = [];        
        if(is_array($data)){
            $resp['content'] =  get_value('content',$data,'');
            $resp['variables'] = get_value('variables',$data,[]);                
            $resp['messages'] = get_value('messages',$data,[]);  
            $resp['selector'] = get_value('selector',$data,'');  
        }else{
            $resp['selector'] = $selector;
            $resp['function'] = $function;
            $resp['message'] = $message;
            $resp['content'] = $data;
        }
        set_response_json($resp);
    	$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($this->data_response_json));
    }
    public function check_logged_in($redirect = 'login'){
        if($this->session->userdata('logged_in')){
            return true;
        }
        $flashdata = $this->session->flashdata();
        $this->session->set_flashdata($flashdata);
        if(!isset($flashdata['redirect'])){
            if($_SERVER['REQUEST_METHOD'] == 'GET' && !isAjax()){                
                $rd = $this->uri->uri_string();
                if(strlen($_SERVER['QUERY_STRING'])){
                    $rd .= '?'.$_SERVER['QUERY_STRING'];
                }
                $this->session->set_flashdata('redirect',$rd);
            }else{
                $this->session->set_flashdata('redirect',current_url());
            }
        }
        if(isAjax()){
            header("HTTP/1.1 401 Unauthorized");
            echo $this->output->set_content_type('application/json')
                ->set_output(json_encode(array('redirect' => $redirect)))
                ->get_output();
            exit;
        }
        redirect($redirect);
        exit;
    }   
    public function log_out()
    {
        $this->session->sess_destroy();
        redirect('');
    }
    function checkPermission($perm){
        if(preg_match('/|/', $perm)){
            $arr = explode('|', $perm);
            foreach ($arr as $value) {
                if($this->m->usuario->checkPermission($value)){
                    return;
                }
            }
        }elseif($this->m->usuario->checkPermission($perm)){
            return;
        }
        $msg = 'Seu usuário não tem acesso a esse conteúdo.';
        $msg .= '<br><a href="'.base_url().'">página inicial<a/>';
        show_error($msg,403,'Acesso negado!');
    }
    function index()
    {
        
        $nameClass = get_class($this); 
        $sufixContent = strtolower($nameClass); 
        $viewContent = $sufixContent.'/list';
        $data['limit'] = $this->objModel->getLimit();
        $offset = $this->objModel->get_offset_by_url();
        if($offset){
            $data['limit'] .= ','.$offset;
        }
        if(!empty($this->where)){
            $data['where'] =$this->where;
        }
        $list = $this->objModel->list_items($data);

        if(isAjax()){
            $content = 
            $this->load->view($viewContent,
                [
                    'sufixContent'=>$sufixContent,
                    'list' => $list,
                    'pagination' => $this->objModel->buildPagination([],['item_uri'=>$sufixContent]),
                ],
                True
            );
            return $this->response_json($content);
        }
        $this->lang->load('sgc','custom');
        $titleContent = $this->lang->line('Lista de '.$nameClass);
        $this->load->view('index',
            [
                'sufixContent'=>$sufixContent,
                'viewContent' => $viewContent,
                'titleContent' => empty($titleContent)?'Lista de '.$nameClass:$titleContent,
                'list' => $list,
                'pagination' => $this->objModel->buildPagination([],['item_uri'=>$sufixContent]),
            ]
        );
    }
    function busca()
    {
        $query = $this->input->get('query');
        if(strlen($query)){
            $this->where[$this->objModel->fieldName.' LIKE|both'] = $query;
        }
        return $this->index();
    }
    function edit($id)
    {
        $post = $this->input->post();
        $sufixContent = strtolower(get_class($this)); 
        $viewContent = $sufixContent.'/form';
        $data = [];
        $data['sufixContent'] = $sufixContent;
        if($post){
            $messages = [];
            if(!$this->objModel->update($post,$id)){                
                $messages[] = ['content'=>'Algumas Informações não foram atualizadas!', 'type'=> 'error'];
            }else{
                $messages[] = ['content'=>'Informações atualizadas!','type'=>'success'];
            }
            $item = $this->objModel->get($id);
            $data = array_merge($data,$this->objModel->get_context_variables($id));         
            $data['item'] = $item;
            return $this->response_json([
                'content'=>$this->load->view($viewContent, $data,True),
                'messages'=>$messages
            ]);
        }
        $this->lang->load('sgc','custom');
        $data = array_merge($data,$this->objModel->get_context_variables($id));         
        $data['item'] = $this->objModel->get($id);
        $data['viewContent'] = $viewContent;
        $data['titleContent'] = $this->lang->line('informacoes_'.strtolower(get_class($this->objModel)));
        $this->load->view('index',$data);
    }
    public function add()
    {
        
        $pessId = $this->objModel->insert([]);
        redirect(strtolower(get_class($this)).'/'.$pessId.'/edit');
    }
    public function remover($id)
    {
        $messages = [];
        $variables = [];
        if($id && $this->objModel->remove($id)){                
            $messages[] = ['content'=>'Removido com sucesso!','type'=>'success'];
        }else{
            $messages[] = ['content'=>'Não foi possível remover ', 'type'=> 'error'];
            $variables = ['error'=>1];
        }
        return 
        $this->response_json([
                'messages'=>$messages,
                'variables'=>$variables,
         ]);
    }
    public function autoloading()
    {
        $post = $this->input->post();
        $sufixContent = strtolower(get_class($this)); 
        $data = ['sufixContent'=>$sufixContent];
        $data['limit'] = 10;
        $offset = $this->objModel->get_offset_by_url();
        if($offset){
            $data['limit'] .= ','.$offset;
        }
        if($post){
            $query = get_value('query',$post);
            $data['order_by'] = "IF(LOCATE('".str_replace("'", "\'", $query)."',{$this->objModel->fieldName}) = 1 ,0,1), {$this->objModel->fieldName}";
            $data['where'] = [$this->objModel->fieldName.' LIKE|both' =>$query];
            $data['list'] = $this->objModel->list_items($data);
            $content = $this->load->view($sufixContent.'/autoloading',$data,True);
        }else{

            $data['list'] = $this->objModel->list_items($data);
            $content = '<div>';
            $content .= '<form class="form-autoloading" action="'.site_url($sufixContent).'/autoloading">';
            $content .= '<div class="form-group">';          
            $content .= '<div class="input-group">';
            $content .= '<input type="text" name="query" class="form-control" autocomplete="off"/>';       
            $content .= '<a class="input-group-addon"><span class="glyphicon glyphicon-search"></span></a>';
            $content .= '</div>';
            $content .= '</div>';
            $content .= '</form>';
            $content .= $this->load->view($sufixContent.'/autoloading',$data,True);
            $content .= '</div>';
        }
        
        return $this->response_json($content);
    }
}