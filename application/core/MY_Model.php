<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
	const STATUS_ACTIVE =  1;
	const STATUS_INACTIVE =  0;
	const STATUS_REMOVED =  -1;
	var $limit =  20;
	var $table = '';
	var $fieldKey = '';
	var $fieldStatus = '';
	var $fields = [];
	var $fields_types =  array(
		'datetime'=>array()
		);
	var $confiWhereSearch = [];
	var $errors = [];
	public function __construct(){
		parent::__construct();
	}
	public function getFieldKey($opt = ['t'=>TRUE]){
		if($opt['t']){
			return $this->fieldKey;
		}else{
			return preg_replace('/^ *[^\.]+\./','', $this->fieldKey);
		}
	}
	public function getLimit(){
		return $this->limit;
	}
	public function getFieldStatus($toAction = ''){
		return $this->fieldStatus;
	}
	public function filterFields($data, $table = ''){
		$ret =  new stdClass();
		if(!empty($this->table) && !empty($data)){
			
			if(!empty($table)){
				$fields = $this->db->list_fields($table);
			}elseif($this->fields == []){
				$this->fields = $fields = $this->db->list_fields($this->table);
			}else{
				$fields = $this->fields;
			}

			foreach ($fields as $field) {
				if(is_array($data)){
					if(array_key_exists($field, $data)){
						$ret->{$field} = $data[$field];
					}
				}elseif(property_exists($data,$field)){
					$ret->{$field} = $data->$field;
				}
			}
		}
		if(count($ret)){
			foreach ($this->fields_types['datetime'] as $value) {
				if(isset($ret->{$value})){
					if(preg_match('/(\d{1,2})\/(\d{1,2})\/(\d{4,}) ?(.*)$/', $ret->{$value},$match) ){
						$ret->{$value} = trim($match[3].'-'.$match[2].'-'.$match[1].' '.$match[4]);
					}else{
						$ret->{$value} = NULL;
					}
				}
			}
		}
		return is_array($data) ? (array)$ret : $ret;
	}
	public function insert($data){
		if($this->is_insert_inactive() && !array_key_exists($this->fieldStatus, $data)){
			$data[$this->fieldStatus] = $this::STATUS_INACTIVE;
		}
		$dataInsert = $this->filterFields($data);
		if($this->db->insert($this->table,$dataInsert)){
			$resp = $this->db->insert_id();
			if(empty($resp) && $this->fieldKey 
				&& array_key_exists($this->fieldKey, (array)$dataInsert )){
				return get_value($this->fieldKey,$dataInsert);
			}
			return $resp;
		}
		return false;
	}
	public function is_insert_inactive(){
		if(!empty($this->fieldStatus)){
			return TRUE;
		}
		return FALSE;
	}
	public function update($data, $where){
		$dataUpdate = $this->filterFields($data);
		if($dataUpdate == []){
			return FALSE;
		}
		$fieldStatus = $this->getFieldStatus('update');

		if(!empty($fieldStatus)){
			if(is_array($dataUpdate) && !array_key_exists($fieldStatus, $dataUpdate)){
				$dataUpdate[$fieldStatus] = $this::STATUS_ACTIVE;
			}elseif(is_object($dataUpdate) && !property_exists($dataUpdate, $fieldStatus)){
				$dataUpdate->{$fieldStatus} = $this::STATUS_ACTIVE;
			}
		}
		if(is_array($where) && $where != []){
			$dataw = ['where'=>$where];
			$this->buildQuery($dataw,['status'=>False]);
		}else{
			$fieldKey = $this->getFieldKey(['t'=>FALSE]);
			$this->db->where([$fieldKey=>$where]);
		}
		return $this->db->update($this->table,$dataUpdate);
	}
	public function remove($where){
		if(is_array($where) && $where != []){
			$data = ['where'=>$where];
			$this->buildQuery($data,['status'=>False]);
		}else{
			$this->db->where(array($this->fieldKey=>$where));
		}
		if(empty($this->fieldStatus)){
			return $this->db->delete($this->table);
		}
		$resp = $this->db->update($this->table,array($this->fieldStatus => $this::STATUS_REMOVED));
		return $resp;
	}
	public function get($where, $data = []){
		if(!is_array($where)){
			$where = ['A.'.$this->fieldKey=>$where];
		}
		$query_status = $this->query_status_get($where);
		if($query_status != []){
			$where = array_merge($where,$query_status);
		}
		$data['where'] = $where;
		return $this->filter($data)->row();
	}
	public function query_status_get($where = []){
		$query = [];
		if(!empty($this->fieldStatus) && !array_key_exists($this->fieldStatus,$where)){
			$st = preg_match('/.+\..+/',$this->fieldStatus) ? $this->fieldStatus : 'A.'.$this->fieldStatus;
			$query['('.$st.' <> '.$this::STATUS_REMOVED.' OR '.$st.' IS NULL)'] = NULL;
		}
		return $query;
	}
	public function filter($data = []){
		$this->buildQuery($data);
		$this->filter_last_info = $data;
		return $this->db->get();
	}
	public function buildQuery(&$data, $options = []){
		if(empty($data['where'])){
			$data['where'] = [];
		}
		$query_status = $this->query_status_get($data['where']);
		$status = get_value('status',$options,True);
		if($status && $this->fieldStatus && !array_key_exists(key($query_status), $data['where'])){
			$st = preg_match('/.+\..+/',$this->fieldStatus) ? $this->fieldStatus : 'A.'.$this->fieldStatus;
			$exist_st = FALSE;
			foreach ($data['where'] as $key => $value) {
				if(preg_match('/ *'.$st.' */',$key)){
					$exist_st = TRUE;
					break;
				}
			}
			if(!$exist_st){
				$data['where'][$st] = $this::STATUS_ACTIVE;
			}
		}
		
		if(isset($data['where']) && $data['where'] != []){
			if(is_array($data['where'])){
				foreach ($data['where'] as $key => $value) {
					if(preg_match('/ IN$/i', $key)){
						$key = preg_replace('/ IN$/i', '', $key);
						$this->db->where_in($key,$value);						
					}elseif(preg_match('/ LIKE$| LIKE\|(before|after|both)/i', $key,$mt)){
						$key = preg_replace('/ LIKE.*$/i', '', $key);
						$this->db->like($key, $value,(isset($mt[1])?$mt[1]:'none'));
					}else{
						$this->db->where($key,$value);
					}
				}
			}else{
				$this->db->where($data['where']);
			}

		}
		if(!empty($data['fields'])){
			$this->db->select($data['fields']);
		}
		if(!empty($data['limit'])){
			$o_l = explode(',', $data['limit']);
			if(count($o_l) == 2){
				$this->db->limit($o_l[0],$o_l[1]);
			}
			$this->db->limit($o_l[0]);
		}
		if(!empty($data['join'])){
			foreach ($data['join']as $join) {
				$this->db->join($join[0], $join[1],(empty($join[2]) ? 'INNER' : $join[2]));
			}
		}
		if(!empty($data['group_by'])){
			$this->db->group_by($data['group_by']);
		}
		if(!empty($data['order_by'])){
			$this->db->order_by($data['order_by']);
		}
		$this->db->from($this->table.' A');
	}
	public function getErros(){
		return $this->errors;
	}
	public function get_cont($data = []){
		if($data == [] & !empty($this->filter_last_info)){
			$data = $this->filter_last_info;
		}				
		unset($data['limit']);
		$pKey = empty($this->fieldKey)|| !is_string($this->fieldKey) ?'*':$this->fieldKey;
		if($pKey !='*'){
			$pKey = preg_match('/^ *.+\./',$pKey) ? $pKey :'A.'.$pKey;
		}
		$data['fields'] = 'count('.$pKey.') AS count';
		$this->buildQuery($data);
		$result = $this->db->get();
		$count = 0;
		if(empty($data['group_by'])){
			$count = get_value('count',$result->row(),0);
		}else{
			$count = $result->num_rows();
		}
		$this->lastCount = $count;
		return $count;
	}
	public function get_offset_by_url(){
		if(preg_match('/page-(\d+)/', $_SERVER['REQUEST_URI'],$match)){
			return (--$match[1] * $this->getLimit());
		}
		return null;
	}
	public function buildPagination($data = [],$config2 = []){
		$this->load->library('pagination');
		$config  = [];
		$config['prefix'] = $prefix = 'page-';
		$config['per_page'] = $this->getLimit();
		if(!array_key_exists('total_rows', $config)){
			$config['total_rows'] = $this->get_cont($data);
		}
		
		if(!empty($data['limit'])){
			$config['per_page'] = trim(preg_replace('/,.*$/', '', $data['limit']));
		}
		
		$config['data_page_attr'] = 'page';
		$config['use_page_numbers'] = TRUE;		
		$config['first_tag_open']  = '<li class="paginate_button previous">';
		$config['first_tag_close'] = '</li>';

		$config['cur_tag_open']  = '<li class="paginate_button active"><a>';
		$config['cur_tag_close'] = '</a></li>';

		$config['prev_tag_open']  = '<li class="paginate_button previous " >';
		$config['prev_tag_close'] = '</li>';

		$config['next_tag_open']  = '<li class="paginate_button next"">';
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li class="paginate_button">';
		$config['num_tag_close'] = '</li>';

		$config['last_tag_open']  = '<li class="paginate_button previous">';
		$config['last_tag_close'] = '</li>';

		if($config2 != []){
			$config = array_merge($config,$config2);
		}
		if(empty($config['base_url'])){
			if( isset($config['item_uri']) && !preg_match('#'.preg_quote($config['item_uri']).'#',$_SERVER['REQUEST_URI'])){
				$config['base_url'] = base_url($config['item_uri']);
			}else{
				$config['base_url'] = current_url();
				if($_SERVER['QUERY_STRING'] !==''){
					$config['base_url'] .= '?'.$_SERVER['QUERY_STRING'];
				}
			}
		}
		if(preg_match('/'.$prefix.'(\d+)/',$config['base_url'],$mt)){
			$config['cur_page'] = $mt[1];
		}
		$config['base_url'] = preg_replace('/\??'.$prefix.'\d+|\??'.$prefix.'|/', '', $config['base_url']);
		if( preg_match('/\?.+&+/', $config['base_url'])){
			$config['base_url'] = rtrim($config['base_url'], '&').'&';
		}
		
		$this->pagination->initialize($config);
		$links = $this->pagination->create_links();
		if(!empty($links)){
			$links = '<ul class="pagination '.(count($this->input->post())?'posted':'').'" >'.$links.'</ul>';
		}
		return $links;
	}
	public function get_list_status(){
		$arr = [];
		#$arr[$this::STATUS_ACTIVE] = lang('ws_adm_fm_status_published');
		#$arr[$this::STATUS_INACTIVE] = lang('ws_adm_fm_status_inactive');
		#$arr[$this::STATUS_REMOVED] = lang('ws_adm_fm_status_removed');
	 	return $arr;
	}
	public function get_context_variables(){
		$variables = [];
		$variables['list_status'] = $this->get_list_status();
		return $variables;
	}
	public function buildWhereSearch($post = []){
		$where = [];
		foreach ($post as $key => $value) {
			if($value ===''){
				continue;
			}
			if(is_array($value)){
				$where[$key.' IN'] = $value;
			}elseif(isset($this->confiWhereSearch[$key])){
				$where[$this->confiWhereSearch[$key]] = $value;
			}else{
				$where[$key] = $value;
			}
		}
		return $where;
	}
	public function list_items($data = []){
		return $this->filter($data);
	}
}