<div class="form-group">
	<label for="engLogradouro">Logradouro</label>
	<input type="text" value="<?= get_value('engLogradouro',$item) ?>" class="form-control" id="engLogradouro" name="engLogradouro" placeholder="Logradouro">
</div>
<div class="row">	
	<div class="form-group col-sm-6">
		<label for="engCidade">Cidade</label>
		<input type="text" value="<?= get_value('engCidade',$item) ?>" class="form-control" id="engCidade" name="engCidade" placeholder="Cidade" >
	</div>
	<div class="form-group  col-sm-6 col-lg-3">
		<label for="endEstado">Estado</label>
		<input type="text" value="<?= get_value('endEstado',$item) ?>" class="form-control" id="endEstado" name="endEstado" placeholder="Estado" >
	</div>
	<div class="form-group  col-sm-6 col-lg-3">
		<label for="endCep">Cep</label>
		<input type="text" value="<?= get_value('endCep',$item) ?>" class="form-control" id="endCep" name="endCep" placeholder="Cep" >
	</div>
</div>