<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Sistema de Gestão de Clínica Médica</title>
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript"> 
      base_url = function(sufix){
          var url = '<?= base_url() ?>';
          if(typeof sufix != 'undefined'){
            url += sufix;
          }
          return url;
      }
    </script>
  </head>
  <body>
    <div class="wrapper">
    <div class="header-bar container-fluid">
      <button type="button" class="navbar-toggle collapsed">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
     </button>
     <a title="Sistema de Gestão de Clínica Médica">SGC</a>
     <ul class="nav navbar-nav">
       <li><a title="Informações do usuário" href="<?= site_url('/usuarios/'.$this->session->userdata('user')->userId.'/edit') ?>"><span class="glyphicon glyphicon-user"></span></a></li>
       <li><a title="Sair" href="<?= site_url() ?>/login/log_out"><span class="glyphicon glyphicon-log-out"></span></a></li>
     </ul>
    </div>
    <div class="">
      <div class="container-menu">
      <ul class="nav nav-sidebar">
        <li >
        <?php if($this->m->usuario->checkPermission('recepcionista') || $this->m->usuario->checkPermission('medico')){ ?>
          <a href="<?= site_url('consultas') ?>"">Consultas</a>
          </li>
          <li><a href="<?= site_url('pacientes') ?>">Pacientes</a></li>
        <?php } ?>
        <?php if($this->m->usuario->checkPermission('administrador')){ ?>
        <li><a href="<?= site_url('funcionarios') ?>">Funcionários</a></li>
        <li><a href="<?= site_url('medicos') ?>">Médicos</a></li>
        <li><a href="<?= site_url('medicamentos') ?>">Medicamentos</a></li>
        <li><a href="<?= site_url('usuarios') ?>">Usuários</a></li>
        <?php } ?>
      </ul>
      </div>
      <div class="container-content">
      <h1 class="content-header"><?= isset($titleContent)? $titleContent : '{Titutlo do Conteudo}' ?></h1>
      <?php 
      if (!isset($viewContent)) {
        $viewContent = '';
      };
      
      ?>
      <div id="content-<?= isset($sufixContent) ? $sufixContent : '' ?>">
        <?php
        if(isset($formBusca)){
          echo $formBusca;
        }elseif(preg_match('/\/list$/', $viewContent)){
          $this->load->view('form_busca',['showAdd'=>True]);
        }
        ?>
        <?php if ($viewContent) include $viewContent.'.php'; ?>
      </div>
      </div>
    </div>
    </div>

    <?php if(isLocal()){ ?>
      <script src="<?= base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
    <?php }else{ ?>
      <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <?php } ?>
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/jQuery-Mask-Plugin-master/jquery.mask.min.js"></script>
    <script src="<?= base_url() ?>assets/js/script.js"></script>
  </body>
</html>
