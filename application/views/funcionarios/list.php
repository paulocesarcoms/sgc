<div id="list-<?= $sufixContent ?>">
	<table class="table table-striped">
  		<tr>
  			<th>NOME</th>
  			<th>CPF</th>
  			<th>RG</th>
        <th>NOME DA MÃE</th>
  			<th></th>
  		</tr>
  	<?php 
  		foreach ($list->result() as $item) {  			
  	?>
  		<tr>
  			<td>
  			<a href="<?= site_url($sufixContent.'/'.$item->pessId)?>/edit"> <?= $item->pessNome ?><a/>
  			</td>
  			<td><?= $item->pessCpf ?></td>
  			<td><?= $item->pessRg ?></td>
  			<td><?= $item->pessNomeMae ?></td>
        <td>
        <span class="remover link glyphicon glyphicon-trash" href="<?= site_url($sufixContent.'/'.$item->pessId.'/remover') ?>"></span>
        </td>
  		</tr>
  	<?php } ?>
	</table>
	<?= $pagination ?>
</div>