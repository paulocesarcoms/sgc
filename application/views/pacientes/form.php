<div class="box-form" id="box-<?= $sufixContent ?>">
	<ul class="nav nav-tabs">
	  <li role="presentation"  class="active"><a data-toggle="tab" href="#info-p">Anformações pessoais</a></li>
	  <li role="presentation"><a data-toggle="tab" href="#consultas">Consultas</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="info-p">
			<form class=" form-xhr" method="post" enctype="multipart/form-data">
			  <?php $this->load->view('pessoas/fields',['data'=> &$data]) ?>
			  <button type="submit" class="btn btn-primary">Salvar</button>
			</form>
		</div>
		<div class="tab-pane" id="consultas">
		<br>
		<?php $this->load->view('consultas/list_consultas',[
			'list'=>$listConsultas,
			'urlRefresh'=>site_url('consultas').'?pas='.get_value('pessId',$item)
		]); ?>
		</div>
	</div>
</div>