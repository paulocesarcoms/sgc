<form class="form-busca" method="get" action="<?= empty($action)? site_url($sufixContent.'/busca') : $action ?>">
  <div class="row">
  <div class="form-group col-sm-6 col-md-4">
    <div class="input-group">
      <input type="text" placeholder="busca" name="query" class="form-control" autocomplete="off"/>
      <a class="input-group-addon"><span class="glyphicon glyphicon-search"></span></a>
    </div>
  </div>
  <?php if(isset($showAdd) && $showAdd){ ?>
  <div class="form-group col-sm-6 col-md-4">
    <a class="btn btn-sm btn-primary add-item" href="<?= site_url($sufixContent) ?>/add" title="Adicionar novo">
      <span class="glyphicon glyphicon-plus"></span>
    </a>
  </div>
  <?php } ?>
  </div>
</form>