<ul id="list-autoloading-<?= $sufixContent ?>" class="list-autoloading">
	<?php foreach ($list->result() as $item) {?>
		<li data-id="<?= $item->medId ?>" class="link item-autoloading">
			<?= $item->medNomeGen ?>
			<?= empty($item->medNomeFab) ? '': ' ('.$item->medNomeFab.')' ?>		
		</li>
	<?php } ?>
</ul>