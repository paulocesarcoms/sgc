<div class="box-form" id="box-<?= $sufixContent ?>">
	<form class=" form-xhr" method="post" enctype="multipart/form-data">
	  <?php 
	  if(!empty($messages)){
	  	 $this->load->view('message_alert',$messages);
	  } 
	  ?>
	  <fieldset>
	  	<div class="row">
		  	<div class="form-group col-sm-6">
			    <label for="medNomeGen">Nome Genérico</label>
			    <input type="text" value="<?= get_value('medNomeGen',$item) ?>" class="form-control" id="medNomeGen" name='medNomeGen'>
		    </div>
		    <div class="form-group col-sm-6">
			    <label for="medNomeFab">Nome de Fabricação</label>
			    <input type="text" value="<?= get_value('medNomeFab',$item) ?>" class="form-control" id="medNomeFab" name='medNomeFab'>
		    </div>
	    </div>	  
	  </fieldset>
	  <br>
	  <button type="submit" class="btn btn-primary">Salvar</button>
	</form>
</div>