<div id="list-<?= $sufixContent ?>">
	<table class="table table-striped">
  		<tr>
  			<th>Nome Generico</th>
        <th>Nome Fabricante</th>
  			<th></th>
  		</tr>
  	<?php 
  		foreach ($list->result() as $item) {  			
  	?>
  		<tr>
  			<td>
  			<a href="<?= site_url($sufixContent.'/'.$item->medId)?>/edit"> <?= $item->medNomeGen ?><a/>
  			</td>
        <td><?= $item->medNomeFab ?></td>
        <td> 
        <span class="remover link glyphicon glyphicon-trash" href="<?= site_url($sufixContent.'/'.$item->medId.'/remover') ?>"></span>
        </td>
  		</tr>
  	<?php } ?>
	</table>
	<?= $pagination ?>
</div>