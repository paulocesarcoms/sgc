<div class="box-form" id="box-<?= $sufixContent ?>">
	<ul class="nav nav-tabs">
	  <li role="presentation"  class="active"><a data-toggle="tab" href="#info-p">Anformações pessoais</a></li>
	  <li role="presentation"><a data-toggle="tab" href="#agenda">Agenda</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="info-p">
			<form class=" form-xhr" method="post" enctype="multipart/form-data">
			  <?php $this->load->view('pessoas/fields',['data'=> &$data]) ?>
			  <fieldset>
			  	<legend>Informações profissionais</legend>
			  	<div class="row">
				  	<div class="form-group col-sm-6">
					    <label for="medCrm">Crm</label>
					    <input type="text" value="<?= get_value('medCrm',$item) ?>" class="form-control" id="medCrm" name='medCrm' placeholder="Crm">
				    </div>
			    </div>
			  	<div class="row">
				    <div class="form-group col-lg-6">
					    <label for="medEsp">Especialidades</label>
					    <textarea class="form-control" id="medEsp" name='medEsp'><?= get_value('medEsp',$item) ?></textarea>
				    </div>
			  	</div>
			  </fieldset>
			  <button type="submit" class="btn btn-primary">Salvar</button>
			</form>
		</div>
		<div class="tab-pane" id="agenda">
		<form class=" form-xhr" method="post" action="<?= site_url($sufixContent.'/'.get_value('pessId',$item)) ?>/agendar_consulta" enctype="multipart/form-data">
		<fieldset>
			<div class="row">			  	
			    <div class="form-group col-sm-3 col-md-2">
				   <label >Data</label>
				   <div class="input-group">
				     <div class="input-group-addon"> <span class="glyphicon glyphicon-calendar" ></span> </div>
				     <input type="text" name="consDt[date]" value="" class="form-control">				     
				   </div>
				</div>
				<div class="form-group col-sm-2 col-md-2">
				   <label>Horário</label>
				   <div class="input-group">
				     <div class="input-group-addon"> <span class="glyphicon glyphicon-time" ></span> </div>
				     <input type="text" name="consDt[time]" value="" class="form-control">			     
				   </div>
				</div>
		    </div>
			<button type="submit" class="btn btn-primary">Adicionar</button>
		</fieldset>
		</form>
		<br>
		<?php		
		$this->load->view('consultas/list',['medId'=>get_value('medId',$item),'list'=>$listAgendamentos,'horariosDisponiveis'=>$listHdisponiveis]);
		?>
		</div>
	</div>
</div>