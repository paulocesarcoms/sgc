<div id="list-<?= $sufixContent ?>">
	<table class="table table-striped">
  		<tr>
  			<th>Nome</th>
  			<th>Crm</th>
        <th>Especialidades</th>
  			<th></th>
  		</tr>
  	<?php 
  		foreach ($list->result() as $item) {  			
  	?>
  		<tr>
  			<td>
  			<a href="<?= site_url($sufixContent.'/'.$item->pessId)?>/edit"> <?= $item->pessNome ?><a/>
  			</td>
  			<td><?= $item->medCrm ?></td>
  			<td><?= $item->medEsp ?></td>
        <td>
        <span class="remover link glyphicon glyphicon-trash" href="<?= site_url($sufixContent.'/'.$item->pessId.'/remover') ?>"></span>
        </td>
  		</tr>
  	<?php } ?>
	</table>
	<?= $pagination ?>
</div>