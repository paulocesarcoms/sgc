<div class="box-form" id="box-<?= $sufixContent ?>">
	<ul class="nav nav-tabs">
	  <li role="presentation"  class="active"><a data-toggle="tab" href="#auth">Autenticação</a></li>
	  <?php
	  if(!empty($item->pessId)){
	  ?>
	  <li role="presentation"><a data-toggle="tab" href="#informacoes-do-usuario">Anformações do Funcionario</a></li>
	  <?php } ;?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="auth">
			<br>
			<div class="box-form" id="box-<?= $sufixContent ?>">
				<form class=" form-xhr" method="post" enctype="multipart/form-data" autocomplete="off">
				<?php
					if(!empty($messages)){
	  	 			$this->load->view('message_alert',$messages);
					}		 
				?>
				<fieldset>					
				<div class="row">
					
				  <div class="form-group col-md-6">
				    <label for="userLogin">Login</label>
				    <input type="text" value="<?= get_value('userLogin',$item) ?>" class="form-control" id="userLogin" name='userLogin' placeholder="Login" autocomplete="off" required>
				  </div>
				  <div class="form-group col-md-6">
				    <label for="userPassword">Senha</label>
				    <!-- Disabling Chrome Autofill--> 
				    <input type="password" style="position: fixed;left: -110%;top:-110%;z-index: -1;" />
				    <input type="password" value="" class="form-control" id="userPassword" name='userPassword' placeholder="Senha" <?= get_value('userPassword',$item) ? '' : 'required' ?> autocomplete="off">
				  </div>
				</div>
				<?php if($this->m->usuario->checkPermission('administrador')){ ?>
				<div class="row">
					<div class="form-group col-md-6">
					<label>Permissões:</label>
					<input type="hidden" name="permissoes_usuario" value="rm">
					<div class="checkbox">
				<?php
					$medId = get_value('medId',$item);
					foreach ($listPermission->result() as $perm) { ?>
				    <label>
				    <?php if(empty($medId) && $perm->permName =='medico'){ ?>
				      <input type="checkbox"  disabled="disabled">
				    <?php }else{ ?>
						<input type="checkbox"  name="permissoes_usuario[]" value="<?= $perm->permId ?>"<?= in_array($perm->permId, $arrPermission)? 'checked="checked"':'' ?>> 
				    <?php } ?>
				       <?= $perm->permName ?>
				    </label>
				<?php } ?>
				  	</div>
					</div>
				</div>
				<?php } ?>
				</fieldset>
				<br>
				<button type="submit" class="btn btn-primary">Salvar</button>
				</form>
			</div>
		</div>
		<div class="tab-pane" id="informacoes-do-usuario">
		<br>
		<?php
		if(!empty($item->pessId)){
			$this->load->view('funcionarios/form',$this->m->funcionario->get_context_variables($item->pessId));
		}
		?>			
		</div>
	</div>
</div>