<div id="list-<?= $sufixContent ?>">
	<table class="table table-striped">
  		<tr>
  			<th>Nome</th>
  			<th>Cpf</th>
  			<th>Rg</th>
        <th>Nome da Mae</th>
  			<th></th>
  		</tr>
  	<?php 
  		foreach ($list->result() as $item) {  			
  	?>
  		<tr>
  			<td>
  			<a href="<?= site_url($sufixContent.'/'.$item->userId)?>/edit" class="link"> 
          <?= $item->userLogin ?>
          <i><?= $item->pessNome ?></i>
        </a>
  			</td>
  			<td><?= $item->pessCpf ?></td>
  			<td><?= $item->pessRg ?></td>
  			<td><?= $item->pessNomeMae ?></td>
        <td>
        <span class="remover link glyphicon glyphicon-trash" href="<?= site_url($sufixContent.'/'.$item->userId.'/remover') ?>"></span>
        </td>
  		</tr>
  	<?php } ?>
	</table>
	<?= $pagination ?>
</div>