<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Sistema de Gestão de Clínica Médica</title>
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
  <div class="login-logo">
    <center>
      <p>Sistema de Gestão de Clínica Médica</p>
    </center>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Faça login para iniciar sua sessão</p>
    <form action="<?= site_url() ?>/login" method="post">
      <?php if($this->input->post()){ ?>
      <div class="form-group has-feedback">
        <div class="alert alert-warning alert-dismissible">
        <h4><i class="icon fa fa-warning"></i>Atenção!</h4>
        <?php echo $this->session->flashdata('validation_errors_login'); ?>
      </div>
      </div>
     <?php } ?>
      <div class="form-group has-feedback">
        <input type="text" name="user_login" value="" class="form-control" placeholder="login" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="user_password" class="form-control" placeholder="Senha" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">        
        <div class="col-xs-8"></div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
        </div>
      </div>
    </form>   

  </div>
  <!-- /.login-box-body -->
</div>
    <?php if(isLocal()){ ?>
      <script src="<?= base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
    <?php }else{ ?>
      <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
    <?php } ?>
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
