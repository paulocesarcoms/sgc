<div id="list-<?= $sufixContent ?>">
  <ul class="nav nav-tabs">
    <li role="presentation"  class="active"><a data-toggle="tab" href="#consultas-m">Consultas marcadas</a></li>
    <li role="presentation"><a data-toggle="tab" href="#consultas-d">Horários disponíveis</a></li>
  </ul>
  <div class="tab-content">
	<div class="tab-pane active" id="consultas-m">
	<br>
	<?php 
	if(isset($item)){
		$pessId = get_value('pessId',$item);
	}
	$this->load->view('form_busca',['action'=>site_url('consultas/busca').(empty($medId)?'':'?med='.$pessId)]);
	$urlRefresh = site_url('consultas').(empty($medId)?'':'?med='.$pessId);
	$this->load->view('consultas/list_consultas',['urlRefresh'=>$urlRefresh]);
	?>
	</div>
	<div class="tab-pane" id="consultas-d">
	<br>
	<?php
	$urlRefresh = site_url('consultas/busca_horarios_disponiveis');
	if(!empty($medId)){
		$urlRefresh .= '?med='.$pessId;
	}
	$this->load->view('form_busca',['action'=>$urlRefresh]);
	if(isset($horariosDisponiveis)){
		$this->load->view('consultas/list_consultas',['urlRefresh'=>$urlRefresh,'list'=>$horariosDisponiveis]);
	}
	?>	
	</div>
  </div>
</div>