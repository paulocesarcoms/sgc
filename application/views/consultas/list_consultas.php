<div id="list-agendamento">
<?php
if(empty($list)){
	$list = &$listAgendamentos;
}
setlocale(LC_TIME, 'pt_BR','pt_BR.utf8','pt_BR.iso88591');
if($list->num_rows()){
	$diaAnt = '*';
	$dia = '';
	?><ul data-refresh="<?= isset($urlRefresh) ? $urlRefresh : site_url($sufixContent) ?>" ><?php 
	foreach ($list->result()as $ag) {
		$arr = explode('-',$ag->consDt);
		if(!empty($arr[2])){
			$dia = $arr[0].$arr[1].preg_replace('/ \d.+$/','', $arr[2]);
		}
		if($dia  != $diaAnt){
			echo '<li class="dia">'.date('d M Y',strtotime($ag->consDt)).'</li>';
		}
		?>
		<li class="hr">
			<span> 
				<span class="glyphicon glyphicon-time"></span>
				<?= preg_replace('/.+ (\d+:\d+):\d+$/','$1',  $ag->consDt) ?>
			</span>
			<div class="timeline-box">
				<span><a href="#"><?= $ag->medicoNome ?></a></span>
				<div class="timeline-body">
                  <?= $ag->pacienteNome ?>
                </div>
                <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs" href="<?= site_url('consultas/'.$ag->consId) ?>/edit">
                  		<span class="glyphicon glyphicon-edit"></span>
                  </a>
                  <span class="btn btn-danger btn-xs">                  	
                  <span class="remover link glyphicon glyphicon-trash" href="<?= site_url('consultas/'.$ag->consId.'/remover') ?>"> </span>                
                  </span>

                </div>
			</div>
		</li>
		<?php
		$diaAnt = $dia;
	}
	echo '</ul>';
}
?>
</div>