<?php
$consDt = get_value('consDt',$item);
$arrDt = ['',''];
if($consDt){	
	$arrDt = explode(' ', format_value($consDt,'datehour'));
}
?>
<div class="box-form" id="box-<?= $sufixContent ?>">
	<form class=" form-xhr" method="post" enctype="multipart/form-data">
		<fieldset>
			<div class="row">			  	
			    <div class="form-group col-sm-3 col-md-2">
				   <label >Data</label>
				   <div class="input-group">
				     <div class="input-group-addon"> <span class="glyphicon glyphicon-calendar" ></span> </div>
				     <input type="text" name="consDt[date]" value="<?= $arrDt[0] ?>" class="form-control" required>	     
				   </div>
				</div>
				<div class="form-group col-sm-3 col-md-2">
				   <label>Horário</label>
				   <div class="input-group">
				     <div class="input-group-addon"> <span class="glyphicon glyphicon-time" ></span> </div>
				     <input type="text" name="consDt[time]" value="<?= $arrDt[1] ?>" class="form-control" required>			     
				   </div>
				</div>
		    </div>
		  	<div class="row">
			<div class="form-group col-sm-6">
				<div class="input-group">	
				    <div class="input-group-addon">Médico</div>
				    <div class="form-control">
				    	<span class="text"><?= get_value('medicoNome',$item) ?></span>
				    	<input type="hidden" name="pessIdMed" value="<?= get_value('pessIdMed',$item) ?>">
			    	</div>
				    <a class="input-group-addon ws-window" href="<?= site_url('medicos/autoloading') ?>"><span class="glyphicon glyphicon-edit"></span></a>
		    	</div>
		    </div>
		    <div class="form-group col-sm-6">
		    	<div class="input-group">		    		
				    <div class="input-group-addon">Paciente</div>
				    <div class="form-control">
				    	<span class="text"><?= get_value('pacienteNome',$item) ?></span>
				    	<input type="hidden" name="pessIdPas" value="<?= get_value('pessIdPas',$item) ?>">
			    	</div>
				    <a class="input-group-addon ws-window" href="<?= site_url('pacientes/autoloading') ?>"><span class="glyphicon glyphicon-edit"></span></a>
		    	</div>
		    </div>
		    </div>
		</fieldset>
		<?php if($this->m->usuario->checkPermission('medico')){ ?>
		<fieldset>
			<legend>Medicamentos</legend>
			<div class="row">
			<?php foreach ($listMedicamentos->result() as $med) { ?>
				<div class="col-md-6 box-medicamento">
				    <a class="ws-window" href="<?= site_url('medicamentos/autoloading') ?>"><span class="glyphicon glyphicon-edit"></span></a>
				    <a class="link trash" data-fields=".fields"><span class="glyphicon glyphicon-trash"></span></a>
					<div class="form-group">
				    	<label class="text">
				    	<?= $med->medNomeGen ?>
				    	<?= empty($med->medNomeFab) ? '': ' ('.$med->medNomeFab.')' ?>			    		
				    	</label>
				    	<input type="hidden" class="fields" value="<?= $med->medId ?>" name="medicamentos[<?= $med->consMedId ?>][medId]">
				    	<textarea class="form-control fields" name="medicamentos[<?= $med->consMedId ?>][consMedDesc]" placeholder="descrição" rows="3"><?= $med->consMedDesc ?></textarea>
				  	</div>
				</div>
			<?php } ?>
				<div class="col-md-6 box-medicamento prototipo">
				    <a class="ws-window" href="<?= site_url('medicamentos/autoloading') ?>"><span class="glyphicon glyphicon-edit"></span></a>
				    <a class="link trash" data-fields=".fields"><span class="glyphicon glyphicon-trash"></span></a>				    
					<div class="form-group">
				    	<label class="text"></label>
				    	<input type="hidden" class="fields" name="medicamentos_new[med_id][]">
				    	<textarea class="form-control fields" name="medicamentos_new[desc][]" placeholder="descrição" rows="3"></textarea>
				  	</div>
				</div>
			</div>
			<a class="btn btn-sm btn-primary medi-add">
				<span class="glyphicon glyphicon-plus"></span>
			</a>
		</fieldset>
		<fieldset>
			<legend>Exames</legend>
			<div class="row">
				<?php foreach ($listExames->result() as $exame) { ?>
				<div class="col-md-6 box-exame">
				    <a class="ws-window" href="<?= site_url('exames/autoloading') ?>"><span class="glyphicon glyphicon-edit"></span></a>
				    <a class="link trash" data-fields=".fields"><span class="glyphicon glyphicon-trash"></span></a>
					<div class="form-group">
				    	<label class="text">
				    	<?= $exame->exaNome ?>				    			    		
				    	</label>
				    	<input type="hidden" class="fields" value="<?= $exame->exaId ?>" name="exames[<?= $exame->consExaId ?>][exaId]">
				    	<textarea class="form-control fields" name="exames[<?= $exame->consExaId ?>][consExaDesc]" placeholder="descrição do resultado" rows="3"><?= $exame->consExaDesc ?></textarea>
				    	<div class="radio">
				    	<?php foreach (['Guardado na clínica','Entregue ao paciente'] as $value) { ?>
						    <label>
						      <input type="radio" value="<?= $value ?>" name="exames[<?= $exame->consExaId ?>][consExaDestino]" class="fields" <?= $exame->consExaDestino == $value ? 'checked="checked"':'' ?> > <?= $value ?>
						    </label>
						<?php } ?>
					  	</div>
				  	</div>
				</div>
				<?php } ?>
				<div class="col-md-6 box-exame prototipo">
				    <a class="ws-window" href="<?= site_url('exames/autoloading') ?>"><span class="glyphicon glyphicon-edit"></span></a>
				    <a class="link trash" data-fields=".fields"><span class="glyphicon glyphicon-trash"></span></a>				    
					<div class="form-group">
				    	<label class="text"></label>
				    	<input type="hidden" class="fields" name="exames_new[exa_id][]">
					</div>
				</div>
			</div>
			<a class="btn btn-sm btn-primary medi-add">
				<span class="glyphicon glyphicon-plus"></span>
			</a>
		</fieldset>
		<?php } ?>
		<br>
		<button type="submit" class="btn btn-primary">Salvar</button>
	</form>
</div>
<?php
