<?php 
$type_error = '';
if(!empty($message_success)){
	$type_error = 'success';
	$message = $message_success;
}
if(!empty($message_warning)){
	$type_error = 'warning';
	$message = $message_warning;
}
if(!empty($message_error)){
	$type_error = 'danger';
	$message = $message_error;
}
if(!empty($type_error)){ ?>
  <div class="alert alert-<?php echo $type_error ?> alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> <?php echo $message; ?></h4>
  </div>
  <?php
}
?>