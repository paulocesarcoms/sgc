<?php
	  if(!empty($messages)){
	  	 $this->load->view('message_alert',$messages);
	  } 
	  ?>
	  <fieldset>
	  <div class="form-group">
	    <label for="pessNome">Nome</label>
	    <input type="text" value="<?= get_value('pessNome',$item) ?>" class="form-control" id="pessNome" name='pessNome' placeholder="Nome" required>
	  </div>
	  <div class="row">
		  <div class="form-group col-sm-6">
		    <label for="pessCpf">Cpf</label>
		    <input type="text" value="<?= get_value('pessCpf',$item) ?>" class="form-control" id="pessCpf" name='pessCpf' placeholder="Cpf">
		  </div>
		  <div class="form-group col-sm-6">
		    <label for="pessRg">RG</label>
		    <input type="text" value="<?= get_value('pessRg',$item) ?>" class="form-control" id="pessRg" name='pessRg' placeholder="Rg">
		  </div>
	  </div> 
	  <div class="form-group">
	    <label for="pessNomeMae">Nome da Mãe</label>
	    <input type="text" value="<?= get_value('pessNomeMae',$item) ?>" class="form-control" id="pessNomeMae" name='pessNomeMae' placeholder="Nome da Mãe">
	  </div>
	  </fieldset>
	  <fieldset>
	  	<legend>Endereço</legend>
	  <?php $this->load->view('endereco/form'); ?>	  
	  </fieldset>
	  <fieldset>
	  	<legend>Contato</legend>
      	<div class="row">
		<div class="form-group input-group add-field col-lg-9">
	        <div class="input-group-btn">
	          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
	          	Adicinar Contato
	            <span class="caret"></span>
	          </button>
	          <ul class="dropdown-menu">
	          <?php foreach ($tipo_contato->result() as $row) { ?>
	            <li><div class="btn " data-type="<?= $row->tContTipo ?>" data-tcid="<?= $row->tContId ?>"><?= $row->tContNome ?></div></li>
	          <?php } ?>	 
	          </ul>
	        </div>
        <!-- /btn-group -->
        	<input type="text" class="form-control">
      	</div>
      	<?php foreach ($contatos->result() as $cont) { ?>
      	<div class="form-group col-lg-6">
		   <div class="input-group">
		     <div class="input-group-addon"><?= $cont->tContNome ?></div>
		     <input type="<?= $cont->tContTipo?>" name="contatos[<?= $cont->pcContId ?>]" value="<?= $cont->pContato ?>" class="form-control">
		     <div class="input-group-addon link trash"><span class=" glyphicon glyphicon-trash"></span></div>
		   </div>
		</div>
      	<?php } ?>
		</div>
	  </fieldset>