<?php
function pre($data,$exit = false){
	echo '<pre>'.print_r($data,1).'</pre>';
	if($exit){
		exit;
	}
}

function isAjax() {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest") {
        return TRUE;
    }
    return FALSE;
}
function isLocal(){
	return preg_match('/^::1|127\.0\.0\.1|^192\.168\.|^172\.16\.|^10\./', $_SERVER['SERVER_ADDR']);
}
function get_value($key, $data,$return = NULL){
	if(preg_match('/^([^\[]+)\[([^\]]+)\](.*)/', $key,$mt)){
		$key = $mt[1];
	}
	if(is_array($data) && array_key_exists($key, $data)){
		$return = $data[$key];
	}elseif(is_object($data) && property_exists ( $data , $key )){
		$return = $data->{$key};
	}
	if(isset($mt[2])){
		return get_value($mt[2].$mt[3],$return);
	}
	return $return;
}

function format_value($value,$format = ''){
	if($format == 'date'){
		$value = preg_replace('/(\d{4,})-(\d{2})-(\d{2}) ?.*$/', '$3/$2/$1', $value);
	}elseif($format == 'datehour'){
		$value = preg_replace('/(\d{4,})-(\d{2})-(\d{2}) (\d{2}:\d{2}).*$/', '$3/$2/$1 $4', $value);
	}elseif($format == 'dateiso'){
		$value = preg_replace('/(\d{1,2})\/(\d{1,2})\/(\d{4}) ?.*$/', '$3-$2-$1', $value);
	}
	return $value;
}
function set_response_json($data = []){
	if($data != []){
		$variables = get_value('variables',$data);
		$content = get_value('content',$data);
		$selector = get_value('selector',$data);
		$function = get_value('function',$data,'replaceWith');		
		$CI =& get_instance();
    	$resp = [
    		'content'=>$content, 
    		'selector'=>$selector,
    		'function'=>$function,
    		'variables'=>$variables
    	];
    	$messages = get_value('messages',$data);
    	if(is_array($messages)){
    		$resp['messages'] = $messages;
    	}else{
    		$resp['message'] = get_value('message',$data);
    	}
    	$CI->data_response_json[] = $resp;
	}
}