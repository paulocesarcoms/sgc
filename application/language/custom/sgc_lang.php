<?php
$lang['informacoes_paciente'] = 'Informações do paciente';
$lang['informacoes_consulta'] = 'Informações da consulta';
$lang['informacoes_funcionario'] = 'Informações do funcionário';
$lang['informacoes_medico'] = 'Informações do médico';
$lang['informacoes_medicamento'] = 'Informações do medicamento';
$lang['informacoes_usuario'] = 'Informações do usuário';
$lang['Lista de Funcionarios'] = 'Lista de Funcionários';
$lang['Lista de Medicos'] = 'Lista de Médicos';