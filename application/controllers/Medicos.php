<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medicos extends My_Controller {
	var $ModelNome = 'Medico';
	function __construct(){
    	parent::__construct();
		$this->checkPermission('administrador');
    }
	function agendar_consulta($pesssId)
	{
		$post = $this->input->post();
		$date = get_value('consDt[date]',$post);
		$time = get_value('consDt[time]',$post);
		$msgs = [];
		if($date && $time){
			$date = format_value($date,'dateiso');
			$data = ['pessIdMed'=>$pesssId,'consDt'=>$date.' '.$time,'consStatus'=>1];
			
			if($this->m->consulta->insert($data)){
				$msgs[] = ['content'=>'Agendamento realizado!','type'=>'success'];
			}else{
				$msgs[] = ['content'=>implode('<br>',$this->m->consulta->getErros()),'type'=>'error'];
			}
		}
		$arr = $this->m->medico->get_context_variables($pesssId);
		$arr['list'] = &$arr['listHdisponiveis'];
		$arr['urlRefresh'] = site_url('consultas/busca_horarios_disponiveis?med='.$pesssId);
		$content = $this->load->view('consultas/list_consultas',$arr,True);
		return $this->response_json(['messages'=>$msgs,'content'=>$content,'selector'=>'#consultas-d #list-agendamento']);
	}
}
