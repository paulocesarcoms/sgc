<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultas extends My_Controller {
	var $ModelNome = 'Consulta';
	function index()
    {
        $this->checkPermission('recepcionista|medico');        
        $nameClass = get_class($this); 
        $sufixContent = strtolower($nameClass); 
        $viewContent = $sufixContent.'/list';
        $data['limit'] = $this->objModel->getLimit();
        $offset = $this->objModel->get_offset_by_url();
        if($offset){
            $data['limit'] .= ','.$offset;
        }
        $med = $this->input->get('med');
        if($med){
            $this->where['pessIdMed'] = $med;
        }
        $pas = $this->input->get('pas');
        if($pas){
            $this->where['pessIdPas'] = $pas;
        }
        if(!empty($this->where)){
            $data['where'] =$this->where;
        }
        $list = $this->objModel->list_items($data);
        if(isAjax()){
            $content = 
            $this->load->view($sufixContent.'/list_consultas',
                [
                    'sufixContent'=>$sufixContent,
                    'urlRefresh' => $this->input->get() ==[]? current_url() : current_url().'?'.http_build_query($this->input->get()),
                    'list' => $list,
                    'pagination' => $this->objModel->buildPagination([],['item_uri'=>$sufixContent]),
                ],
                True
            );
            return $this->response_json($content);
        }
        unset($data['where']);
        $data['where']['pessIdPas IS NULL'] = Null;
        $data['join'][] = ['tb_pessoas M', 'M.pessId = A.pessIdMed'];
        $data['fields'] = 'A.*, M.pessNome AS medicoNome'.",'' AS pacienteNome";        
        $this->load->view('index',
            [
                'formBusca'=>'',
                'sufixContent'=>$sufixContent,
                'viewContent' => $viewContent,
                'titleContent' => 'Lista de '.$nameClass,
                'list' => $list,
                'horariosDisponiveis' => $this->objModel->filter($data),
                'pagination' => $this->objModel->buildPagination([],['item_uri'=>$sufixContent]),
            ]
        );
    }
    function busca_horarios_disponiveis()
    {
    	$data = [];
    	$query = $this->input->get('query');
        if(strlen($query)){
            $data['where']['M.pessNome LIKE|both'] = $query;
        }
        $med = $this->input->get('med');
        if($med){
            $data['where']['pessIdMed'] = $med;
        }
        $data['where']['pessIdPas IS NULL'] = Null;
        $data['join'][] = ['tb_pessoas M', 'M.pessId = A.pessIdMed'];
        $data['fields'] = 'A.*, M.pessNome AS medicoNome'.",'' AS pacienteNome";

        $sufixContent = strtolower(get_class($this));
        $content = 
        $this->load->view($sufixContent.'/list_consultas',
            [
                'list' => $this->objModel->filter($data),
                'sufixContent' => $sufixContent,
                'urlRefresh' => site_url($sufixContent.'/busca_horarios_disponiveis').(empty($med)?'':'?med='.$med),
                'pagination' => $this->objModel->buildPagination([],['item_uri'=>$sufixContent]),
            ],
            True
        );
        return $this->response_json($content,'#consultas-d #list-agendamento');
    }
}

