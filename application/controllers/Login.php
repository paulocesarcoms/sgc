
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends My_Controller {

	public function index()
	{
		$this->load->library('form_validation');
		$post = $this->input->post();
		if(!$post){
			$this->load->view('login');
			return;
		}
		$this->form_validation->set_rules('user_login', 'Login', 'required');
		$this->form_validation->set_rules('user_password', 'Senha', 'required');
		$redirect = trim(get_value('redirect',$post,''),'/');
		if($this->form_validation->run()){
			$user_login = get_value('user_login',$post);
			$password = get_value('user_password',$post);
			if($this->m->usuario->login(['user_login'=>$user_login,'password'=>$password])){				
				if($redirect && $redirect != 'login'){
					redirect($redirect);
				}else{
					redirect('');
				}
				return;
			}else{
				if($redirect && $redirect != 'login'){
					$this->session->set_flashdata('redirect',$redirect);
				}
				$this->session->set_flashdata('validation_errors_login','Verifique suas credenciais de login.');
			}
		}else{
			$this->session->set_flashdata('validation_errors_login',validation_errors('<div>','</div>'));
		}
		$this->load->view('login');
	}
	function check_logged_in($redirect = 'login'){

	}
}
