<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medicamentos extends My_Controller {
	var $ModelNome = 'Medicamento';
	function __construct(){
    	parent::__construct();
		$this->checkPermission('administrador|medico');
    }
}
