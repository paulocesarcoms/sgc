<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionarios extends My_Controller {
	var $ModelNome = 'Funcionario';
	function __construct(){
    	parent::__construct();
		$this->checkPermission('administrador');
    }
}