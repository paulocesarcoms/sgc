<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_Controller {
	var $ModelNome = 'Consulta';
	function index()
    {
    	$arrUrl = [];
    	$arrUrl['consultas'] = ['recepcionista','medico'];
    	$arrUrl['usuarios'] = ['administrador'];
    	foreach ($arrUrl as $url => $arrPerm) {
    		foreach ($arrPerm as $perm) {
	    		if($this->m->usuario->checkPermission($perm)){
	    			redirect($url);
	            	return;
	    		}
    		}
    	}
    }
}