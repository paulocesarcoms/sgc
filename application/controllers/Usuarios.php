<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends My_Controller {
	var $ModelNome = 'Usuario';
	function __construct(){
    	parent::__construct();
    	$id = get_value('userId',$this->session->userdata('user'));
    	$patt = preg_quote(site_url('usuarios/'.$id.'/'));
    	if(!preg_match('#^'.$patt.'#', current_url())){
			$this->checkPermission('administrador');
    	}
    }
}
