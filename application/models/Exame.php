<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exame extends MY_Model {
	var $table = 'tb_exames';
	var $fieldKey = 'exaId';
	var $fieldName = 'exaNome';
		
	public function __construct(){
		parent::__construct();
	}
}