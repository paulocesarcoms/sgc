<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissoes_usuario extends MY_Model {
	var $table = 'tb_permissoes_usuario';
	var $fieldKey = 'permUserId';
	public function __construct(){
		parent::__construct();
	}
}