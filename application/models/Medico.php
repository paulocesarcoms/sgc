<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'Funcionario.php';
/**
* 
*/
class Medico extends Funcionario
{
	public function filter($data = [])
	{
		$data['join'][] = ['tb_medicos M', 'M.pessId = A.pessId', 'INNER'];
		return parent::filter($data);
	}

	public function insert($data){
		if($this->db->insert('tb_usuario',['userStatus'=>1,'userDtCreate'=>date('Y-m-d H:i:s')])){
			$data['userId'] = $this->db->insert_id();
		}
		$resp = parent::insert($data);
		if($resp){
			$this->db->insert('tb_medicos',['pessId'=>$resp]);
		}
		return $resp;
	}
	public function update($data,$id){
		if($id){
			$dm = $this->filterFields($data,'tb_medicos');
			if($dm != []){
				$this->db->update('tb_medicos',$dm,['pessId'=>$id]);
			}
		}
		return parent::update($data,$id);

	}
	function get_context_variables($id = '',$data = []){
		$arr = parent::get_context_variables($id ,$data);
		if($id){
			$dF = ['where'=>['pessIdMed'=>$id]];
			$dF['join'] = [['tb_pessoas M','M.pessId=A.pessIdMed']];
			$dF['join'][] = ['tb_pessoas P','P.pessId=A.pessIdPas','LEFT'];
			$dF['fields'] = 'A.*, M.pessNome AS medicoNome, P.pessNome AS pacienteNome';
			$dF['where']['pessIdPas IS NOT NULL'] = Null;
			$arr['listAgendamentos'] = $this->m->consulta->filter($dF);
			unset($dF['where']['pessIdPas IS NOT NULL']);
			$dF['where']['pessIdPas IS NULL'] = Null;
			$arr['listHdisponiveis'] = $this->m->consulta->filter($dF);
		}
		return $arr;
	}
}