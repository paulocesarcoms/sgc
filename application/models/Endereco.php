<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Endereco extends MY_Model {
	var $table = 'tb_endereco';
	var $fieldKey = 'endId';
	var $fieldStatus = 'endStatus';
		
	public function __construct(){
		parent::__construct();
	}
}