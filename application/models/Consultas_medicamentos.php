<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultas_medicamentos extends MY_Model {
	var $table = 'tb_consultas_medicamentos';
	var $fieldKey = 'consMedId';
		
	public function __construct(){
		parent::__construct();
	}
}