<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultas_exames extends MY_Model {
	var $table = 'tb_consultas_exames';
	var $fieldKey = 'consExaId';
		
	public function __construct(){
		parent::__construct();
	}
}