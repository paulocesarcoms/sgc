<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissoes extends MY_Model {
	var $table = 'tb_permissoes';
	var $fieldKey = 'permId';
	public function __construct(){
		parent::__construct();
	}
}