<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pessoas_contato extends MY_Model {
	var $table = 'tb_pessoas_contato';
	var $fieldKey = 'pcContId';
		
	public function __construct(){
		parent::__construct();
	}
}