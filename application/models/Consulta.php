<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consulta extends MY_Model {
	var $table = 'tb_consultas';
	var $fieldKey = 'consId';
	var $fieldStatus = 'consStatus';
	var $fieldName = "CONCAT(COALESCE(M.pessNome,''),COALESCE(P.pessNome,''))";
	
	public function __construct(){
		parent::__construct();
	}

	function filter($data = []){
		$data['order_by'] = 'consDt DESC';
		return parent::filter($data);
	}
	function list_items($data = []){
		if(empty($data['where']) || preg_grep('/pessIdMed|pessIdPas|M\.pessNome|P\.pessNome/',array_keys($data['where'])) != []){
			$data['join'][] = ['tb_pessoas M', 'M.pessId = A.pessIdMed'];
			$data['join'][] = ['tb_pessoas P', 'P.pessId = A.pessIdPas'];
			$data['fields'] = empty($data['fields'])? 'A.*, M.pessNome AS medicoNome, P.pessNome AS pacienteNome' :$data['fields'].', M.pessNome AS medicoNome, P.pessNome AS pacienteNome';
		}
		return parent::list_items($data);
	}
	function update($data, $where)
	{
		$consDt = get_value('consDt',$data);
		if(is_array($consDt)){
			if(empty($consDt['date']) || empty($consDt['time'])){
				return False;
			}
			$data['consDt'] = format_value($consDt['date'],'dateiso').' '.$consDt['time'];
		}
		if(array_key_exists('pessIdPas', $data) && empty($data['pessIdPas'])){
			$data['pessIdPas'] = NULL;
		}
		$resp = parent::update($data, $where);

		$medicamentos = get_value('medicamentos',$data);
		if(is_array($medicamentos) && is_numeric($where)){
			foreach ($medicamentos as $consMedId => $dataMed) {
				$this->m->consultas_medicamentos->update($dataMed,['consMedId'=>$consMedId,'consId'=>$where]);
			}
		}
		$exames = get_value('exames',$data);
		if(is_array($exames) && is_numeric($where)){
			foreach ($exames as $consExaId => $dataExa) {
				$this->m->consultas_exames->update($dataExa,['consExaId'=>$consExaId,'consId'=>$where]);
			}
		}
		
		$medicamentos_new = get_value('medicamentos_new',$data);
		if(is_array($medicamentos_new) && is_numeric($where)){
			$med_id = get_value('med_id',$medicamentos_new);
			$desc = get_value('desc',$medicamentos_new);
			if(count($med_id) != count($desc)){
				return False;
			}
			foreach ($med_id as $km => $medId) {
				if(empty($medId) || !is_numeric($medId)){
					continue;
				}
				$this->m->consultas_medicamentos->insert(['consId'=>$where,'medId'=>$medId,'consMedDesc'=>$desc[$km]]);
			}
		}
		$exames_new = get_value('exames_new',$data);
		if(is_array($exames_new) && is_numeric($where)){
			$exa_id = get_value('exa_id',$exames_new);
			$desc = get_value('desc',$exames_new);
			$consExaDestino = get_value('consExaDestino',$exames_new);
			if(count($exa_id)){
				foreach ($exa_id as $km => $exaId) {
					if(empty($exaId) || !is_numeric($exaId)){
						continue;
					}
					$this->m->consultas_exames->insert(['consId'=>$where,'exaId'=>$exaId,'consExaDesc'=>$desc[$km],'consExaDestino'=>$consExaDestino[$km]]);
				}
			}
		}
		
		$medicamentos_trash = get_value('medicamentos_trash',$data);
		if(is_array($medicamentos_trash) && is_numeric($where)){
			foreach ($medicamentos_trash as $consMedId => $dataMed) {
				$this->m->consultas_medicamentos->remove(['consMedId'=>$consMedId,'consId'=>$where]);
			}
		}
		$exames_trash = get_value('exames_trash',$data);
		
		if(is_array($exames_trash) && is_numeric($where)){
			foreach ($exames_trash as $consExaId => $dataMed) {
				$this->m->consultas_exames->remove(['consExaId'=>$consExaId,'consId'=>$where]);
			}
		}

		return $resp;
	}
	function insert($data)
	{
		$consDt = get_value('consDt',$data);
		if($consDt){
			$pessIdMed = get_value('pessIdMed',$data);
			$pessIdPas = get_value('pessIdPas',$data);
			$w = [];
			if($pessIdMed){
				$w[] = 'pessIdMed = '.$pessIdMed;
			}
			if($pessIdPas){
				$w[] = 'pessIdPas = '.$pessIdPas;
			}
			if($w != [] && $this->filter(['where'=>['consDt'=>$consDt, '('.implode(' OR ', $w).')'=>Null]])->num_rows()){
				;
				$this->errors[] = 'Já existe um agendamento para esta data';
				return Null;
			}
		}
		return parent::insert($data);
	}
	function get($where, $data = []){
		
		$data['join'][] = ['tb_pessoas M', 'M.pessId = A.pessIdMed', 'LEFT'];
		$data['join'][] = ['tb_pessoas P', 'P.pessId = A.pessIdPas', 'LEFT'];
		$data['fields'] = 'A.*, M.pessNome AS medicoNome, P.pessNome AS pacienteNome';
		return parent::get($where, $data);
	}
	function get_context_variables($id = '',$data = []){
		$arr = parent::get_context_variables($id ,$data);
		if($id){
			$join = [['tb_medicamentos M','M.medId=A.medId']];
			$arr['listMedicamentos'] = $this->m->consultas_medicamentos->filter(['where'=>['consId'=>$id],'join'=>$join]);

			$join = [['tb_exames M','M.exaId=A.exaId']];
			$arr['listExames'] = $this->m->consultas_exames->filter(['where'=>['consId'=>$id],'join'=>$join]);	
		}
		return $arr;
	}
}