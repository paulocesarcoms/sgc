<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'Pessoa.php';
/**
* 
*/
class Paciente extends Pessoa
{
	var $groupIds = [ 1 ];
	function get_context_variables($id = '',$data = []){
		$arr = parent::get_context_variables($id ,$data);
		if($id){
			$dF = ['where'=>['pessIdPas'=>$id]];
			$dF['join'] = [['tb_pessoas P','P.pessId=A.pessIdPas']];
			$dF['join'][] = ['tb_pessoas M','M.pessId=A.pessIdMed','LEFT'] ;
			$dF['fields'] = 'A.*, M.pessNome AS medicoNome, P.pessNome AS pacienteNome';
			$arr['listConsultas'] = $this->m->consulta->filter($dF);

		}
		return $arr;
	}	
}