<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MY_Model {
	var $table = 'tb_usuario';
	var $fieldKey = 'userId';
	var $fieldStatus = 'userStatus';
	var $fieldName = "CONCAT(COALESCE(pessNome,''),COALESCE(userName,''),' ',COALESCE(userLogin,''))";
	var $listLabel = [];
		
	public function __construct(){
		parent::__construct();
	}
	public function insert($data = []){
		$data['userDtCreate'] = date('Y-m-d H:i:s');
		$resp = parent::insert($data);
		if($resp){
			$this->m->funcionario->insert(['userId'=>$resp]);
		}
		return $resp;
	}
	public function filter($data = [])
	{
		$data['join'][] = ['tb_pessoas P', 'P.userId = A.userId', 'LEFT'];
		$data['join'][] = ['tb_medicos M', 'M.pessId = P.pessId', 'LEFT'];
		$data['fields'] = empty($data['fields'])? 'M.*, P.*, A.*' : $data['fields'].', A.userId';
		return parent::filter($data);
	}
	public function check_login_info($data, $where , $options = []){
		$setErros = get_value('setErros',$options);
		$userLogin = get_value('userLogin',$data);
		$userEmail = get_value('userEmail',$data);

		if(is_numeric($where)){
			if(!empty($userLogin)){
				$this->db->select('userLogin')
					->from($this->table)
					->group_by('userLogin')
					->limit(1)
					->where($this->fieldStatus.' >',self::STATUS_REMOVED)
					->where($this->fieldKey.' <>',$where)
					->group_start();
				if(!empty($userLogin)){
					$this->db->where('userLogin',$userLogin);
				}
				
				$result = $this->db->group_end()->get();
				if($setErros){
					foreach ($result->result() as $row) {
						if(!empty($userLogin) && $row->userLogin == $userLogin){
							$this->errors[] = sprintf(lang('userLogin_already_exists'),$row->userLogin);
						}						
					}					
				}
				return !$result->num_rows();				
			}
			return True;
		}
		return False;
	}
	public function update($data, $where){
		if(!$this->check_login_info($data, $where ,['setErros'=>True])){
			return False;
		}
		if(empty($data['userPassword'])){
			unset($data['userPassword']);
		}elseif(array_key_exists('userPassword', $data) && !empty($data['userPassword'])){
			$this->load->library('encryption');
			$data['userPassword'] = $this->encryption->encrypt($data['userPassword']);
		}
		$resp = parent::update($data, $where);
		$arrPermId = get_value('permissoes_usuario',$data);
		if(is_numeric($where)){
			if(is_array($arrPermId)){
				$arrPermission = $this->getPermissions($where);
				$rm = array_diff($arrPermission,$arrPermId);
				if($rm != []){
					$this->m->permissoes_usuario->remove([$this->fieldKey=>$where,'permId IN'=>$rm]);
				}
				$add = array_diff($arrPermId,$arrPermission);
				if($add != []){
					foreach ($add as $PermId) {
						$this->m->permissoes_usuario->insert(['permId'=>$PermId, $this->fieldKey=>$where]);
					}
				}
			}elseif($arrPermId == 'rm'){
				$this->m->permissoes_usuario->remove([$this->fieldKey=>$where]);
			}			
		}

		return $resp;
	}
	public function login($post = []){
		$this->load->library('encryption');
		$user_login = get_value('user_login',$post);
		if(empty($user_login)){
			return FALSE;
		}
		$password = get_value('password',$post);
		$user = self::filter([
			'limit'=>1,
			'where'=>['userStatus'=>self::STATUS_ACTIVE, 'userLogin'=>$user_login]
		])->row();
		$userId = get_value('userId',$user);
		$userPassword = get_value('userPassword',$user);
		if($userId && $userPassword && $password == $this->encryption->decrypt($userPassword)){
			$userSession = new stdClass();
			$userSession->userId = get_value('userId',$user);
			$userSession->userName = get_value('userName',$user);
			$userSession->userEmail = get_value('userEmail',$user);
			$userSession->userLogin = get_value('userLogin',$user);
			$userSession->userSuper = get_value('userSuper',$user);
			$userSession->userType = get_value('userType',$user);

			#$this->session->sess_destroy();
			$this->session->set_userdata('user',$userSession);
			$this->session->set_userdata('logged_in',TRUE);

			$this->load->library('user_agent');
			$dataUser = [];
			$dataUser['userLastIp'] = $this->input->ip_address();
			$dataUser['userLastBrowser'] = $this->agent->browser().' '.$this->agent->version();
			$dataUser['userLastLogin'] = date('Y-m-d H:i:s');
			self::update($dataUser,$userId);
			return TRUE;
		}
		return FALSE;
	}
	public function getPermissions($userId){
		if(empty($userId)){
			return [];
		}
		$arr = [];
		
		$data = [];
		$data['where'] = array($this->fieldKey=>$userId);
		foreach ($this->m->permissoes_usuario->filter($data)->result() as $value) {
			$arr[get_value('permId',$value)] = get_value('permId',$value);
		}
		return $arr;
	}
	public function get_context_variables($id = '',$data = []){
		$arr = parent::get_context_variables($id ,$data);
		$arr['arrPermission'] = [];
		$this->load->library('encryption');
		if($id){
			$arr['arrPermission'] = $this->m->usuario->getPermissions($id);
			$arr['listPermission'] = $this->m->permissoes->filter();
		
		}
		return $arr;
	}
	
	public function addPermissionCorrentUser($id){
		if(empty($this->permissionCorrentUser[$id])){
			$this->permissionCorrentUser[$id] = $id;
		}
	}
	public function checkPermission($permName){
		if(get_value('userSuper',$this->session->userdata('user'))){
			return True;
		}
		if($this->listLabel == []){
			foreach ($this->m->permissoes->filter()->result() as $value) {
				$this->listLabel[$value->permName] = $value->permId;
			}
		}
		$id = get_value($permName,$this->listLabel);
		if($id){
			$userId = get_value('userId',$this->session->userdata('user'));
			if($userId){
				if(empty($this->permissionCorrentUser)){
					$this->permissionCorrentUser = $this->getPermissions($userId);
				}
				return !empty($this->permissionCorrentUser[$id]);
			}
		}
		return FALSE;
	}
	
	public function change_password($data,$id){
		$password = get_value('password',$data);
        $password_new = get_value('password_new',$data);
        $password_new_confirm = get_value('password_new_confirm',$data);
        if($id && count($password_new) && $password_new == $password_new_confirm){
        	$this->load->library('encryption');
        	$user = $this->get($id);
        	$userPassword = get_value('userPassword', $user);        	
            if($userPassword && $password == $this->encryption->decrypt($userPassword)){
				return $this->update(array('userPassword'=>$password_new),$id);
            }
        }
        return FALSE;
	}
	public function query_status_get($where = []){
		return $where;
	}
}