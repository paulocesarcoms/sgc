<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pessoa extends MY_Model {
	var $table = 'tb_pessoas';
	var $fieldKey = 'pessId';
	var $fieldStatus = 'pessStatus';
	var $fieldName = 'pessNome';
	var $groupIds = [];
		
	public function __construct(){
		parent::__construct();
	}
	public function filter($data = [])
	{
		if($this->groupIds != []){
			$data['join'][] = ['tb_pessoas_grupos PG', 'PG.pessId = A.pessId', 'INNER'];
			$data['where']['grId IN'] = $this->groupIds;
			$data['group_by'] = 'A.pessId';
		}
		$data['join'][] = ['tb_endereco EN', 'EN.endId = A.endId', 'LEFT' ];
		return parent::filter($data);
	}
	public function insert($data){
		$data['endId'] = $this->m->endereco->insert(['endStatus'=>\Endereco::STATUS_ACTIVE]);
		$resp = parent::insert($data);
		if($resp && $this->groupIds != []){
			foreach ($this->groupIds as $grId) {
				$this->db->insert('tb_pessoas_grupos',['grId'=>$grId,'pessId'=>$resp]);
			}
		}
		return $resp;
	}
	public function update($data,$id){
		$resp = parent::update($data,$id);
		if($id){
			$contact_new = get_value('contact_new',$data);
			if(is_array($contact_new)){
				foreach ($contact_new as $key => $arrValue) {
					if(is_numeric($key)){
						foreach ($arrValue as $value) {
							$this->m->pessoas_contato->insert(['pContato'=>$value,'tContId'=>$key,'pessId'=>$id]);
						}
					}
				}
			}
			$item = $this->get($id);
			$endId = get_value('endId',$item);
			if($endId){
				$this->m->endereco->update($data,$endId);
			}
			$contatos = get_value('contatos',$data);
			if(is_array($contatos)){
				foreach ($contatos as $pcContId => $vc) {
					if(is_numeric($pcContId)){
						$this->m->pessoas_contato->update(['pContato'=>$vc],$pcContId);
					}
				}
			}
			$contatos_trash = get_value('contatos_trash',$data);
			if(is_array($contatos_trash)){
				foreach ($contatos_trash as $pcContId => $vc) {
					if(is_numeric($pcContId)){
						$this->m->pessoas_contato->remove($pcContId);
					}
				}
			}
		}
		return $resp;
		
	}
	public function get_context_variables($id = '',$data = []){
		$arr = parent::get_context_variables($id ,$data);
		if($id){
			$dataP = ['where'=>['pessId'=>$id],'order_by'=>'A.tContId'];
			$dataP['join'] = [[ 'tb_tipo_contato TC','TC.tContId=A.tContId','LEFT' ]];
			$arr['contatos'] = $this->m->pessoas_contato->filter($dataP);
			$arr['tipo_contato'] = $this->m->tipo_contato->filter();
		}
		return $arr;
	}
	
}