<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medicamento extends MY_Model {
	var $table = 'tb_medicamentos';
	var $fieldKey = 'medId';
	var $fieldStatus = 'medStatus';
	var $fieldName = "CONCAT(medNomeGen,' ',medNomeFab)";
		
	public function __construct(){
		parent::__construct();
	}
	
	public function insert($data){
		if(!array_key_exists('medNomeGen', $data)){
			$data['medNomeGen'] = '';
		}
		return parent::insert($data);
	}
}